/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-kubernetes',
      type: 'Kubernetes',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Kubernetes = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Kubernetes Adapter Test', () => {
  describe('Kubernetes Class Tests', () => {
    const a = new Kubernetes(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('kubernetes'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('kubernetes'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Kubernetes', pronghornDotJson.export);
          assert.equal('Kubernetes', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-kubernetes', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('kubernetes'));
          assert.equal('Kubernetes', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-kubernetes', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-kubernetes', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getCoreAPIVersions - errors', () => {
      it('should have a getCoreAPIVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getCoreAPIVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCoreV1APIResources - errors', () => {
      it('should have a getCoreV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getCoreV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ComponentStatus - errors', () => {
      it('should have a listCoreV1ComponentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ComponentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1ComponentStatus - errors', () => {
      it('should have a readCoreV1ComponentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1ComponentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ConfigMapForAllNamespaces - errors', () => {
      it('should have a listCoreV1ConfigMapForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ConfigMapForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1EndpointsForAllNamespaces - errors', () => {
      it('should have a listCoreV1EndpointsForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1EndpointsForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1EventForAllNamespaces - errors', () => {
      it('should have a listCoreV1EventForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1EventForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1LimitRangeForAllNamespaces - errors', () => {
      it('should have a listCoreV1LimitRangeForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1LimitRangeForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1Namespace - errors', () => {
      it('should have a listCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1Namespace - errors', () => {
      it('should have a createCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1Namespace(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1Namespace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedBinding - errors', () => {
      it('should have a createCoreV1NamespacedBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedBinding(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedConfigMap - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedConfigMap - errors', () => {
      it('should have a listCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedConfigMap - errors', () => {
      it('should have a createCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedConfigMap(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedConfigMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedConfigMap - errors', () => {
      it('should have a deleteCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedConfigMap - errors', () => {
      it('should have a readCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedConfigMap - errors', () => {
      it('should have a patchCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedConfigMap(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedConfigMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedConfigMap - errors', () => {
      it('should have a replaceCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedConfigMap(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedConfigMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedEndpoints - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedEndpoints - errors', () => {
      it('should have a listCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedEndpoints - errors', () => {
      it('should have a createCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedEndpoints(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedEndpoints - errors', () => {
      it('should have a deleteCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedEndpoints - errors', () => {
      it('should have a readCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedEndpoints - errors', () => {
      it('should have a patchCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedEndpoints(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedEndpoints - errors', () => {
      it('should have a replaceCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedEndpoints(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedEvent - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedEvent - errors', () => {
      it('should have a listCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedEvent - errors', () => {
      it('should have a createCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedEvent - errors', () => {
      it('should have a deleteCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedEvent - errors', () => {
      it('should have a readCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedEvent - errors', () => {
      it('should have a patchCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedEvent(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedEvent - errors', () => {
      it('should have a replaceCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedLimitRange - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedLimitRange - errors', () => {
      it('should have a listCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedLimitRange - errors', () => {
      it('should have a createCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedLimitRange(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedLimitRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedLimitRange - errors', () => {
      it('should have a deleteCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedLimitRange - errors', () => {
      it('should have a readCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedLimitRange - errors', () => {
      it('should have a patchCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedLimitRange(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedLimitRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedLimitRange - errors', () => {
      it('should have a replaceCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedLimitRange(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedLimitRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedPersistentVolumeClaim - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a listCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a createCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedPersistentVolumeClaim(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedPersistentVolumeClaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a deleteCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a readCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a patchCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedPersistentVolumeClaim(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedPersistentVolumeClaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a replaceCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedPersistentVolumeClaim(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedPersistentVolumeClaim', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPersistentVolumeClaimStatus - errors', () => {
      it('should have a readCoreV1NamespacedPersistentVolumeClaimStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPersistentVolumeClaimStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedPersistentVolumeClaimStatus - errors', () => {
      it('should have a patchCoreV1NamespacedPersistentVolumeClaimStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedPersistentVolumeClaimStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedPersistentVolumeClaimStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedPersistentVolumeClaimStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedPersistentVolumeClaimStatus - errors', () => {
      it('should have a replaceCoreV1NamespacedPersistentVolumeClaimStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedPersistentVolumeClaimStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedPersistentVolumeClaimStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedPersistentVolumeClaimStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedPod - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedPod - errors', () => {
      it('should have a listCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedPod - errors', () => {
      it('should have a createCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedPod(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedPod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedPod - errors', () => {
      it('should have a deleteCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPod - errors', () => {
      it('should have a readCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedPod - errors', () => {
      it('should have a patchCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedPod(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedPod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedPod - errors', () => {
      it('should have a replaceCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedPod(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedPod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedPodAttach - errors', () => {
      it('should have a connectCoreV1GetNamespacedPodAttach function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedPodAttach === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedPodAttach - errors', () => {
      it('should have a connectCoreV1PostNamespacedPodAttach function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedPodAttach === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedPodBinding - errors', () => {
      it('should have a createCoreV1NamespacedPodBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedPodBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedPodBinding(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedPodBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedPodEviction - errors', () => {
      it('should have a createCoreV1NamespacedPodEviction function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedPodEviction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedPodEviction(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedPodEviction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedPodExec - errors', () => {
      it('should have a connectCoreV1GetNamespacedPodExec function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedPodExec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedPodExec - errors', () => {
      it('should have a connectCoreV1PostNamespacedPodExec function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedPodExec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPodLog - errors', () => {
      it('should have a readCoreV1NamespacedPodLog function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPodLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedPodPortforward - errors', () => {
      it('should have a connectCoreV1GetNamespacedPodPortforward function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedPodPortforward === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedPodPortforward - errors', () => {
      it('should have a connectCoreV1PostNamespacedPodPortforward function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedPodPortforward === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1DeleteNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1GetNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1HeadNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1OptionsNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1PatchNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1PostNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNamespacedPodProxy - errors', () => {
      it('should have a connectCoreV1PutNamespacedPodProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNamespacedPodProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1DeleteNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1GetNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1HeadNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1OptionsNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1PatchNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1PostNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNamespacedPodProxyWithPath - errors', () => {
      it('should have a connectCoreV1PutNamespacedPodProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNamespacedPodProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPodStatus - errors', () => {
      it('should have a readCoreV1NamespacedPodStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPodStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedPodStatus - errors', () => {
      it('should have a patchCoreV1NamespacedPodStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedPodStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedPodStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedPodStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedPodStatus - errors', () => {
      it('should have a replaceCoreV1NamespacedPodStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedPodStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedPodStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedPodStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedPodTemplate - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a listCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a createCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedPodTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedPodTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a deleteCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a readCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a patchCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedPodTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedPodTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a replaceCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedPodTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedPodTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedReplicationController - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedReplicationController - errors', () => {
      it('should have a listCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedReplicationController - errors', () => {
      it('should have a createCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedReplicationController(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedReplicationController', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedReplicationController - errors', () => {
      it('should have a deleteCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedReplicationController - errors', () => {
      it('should have a readCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedReplicationController - errors', () => {
      it('should have a patchCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedReplicationController(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedReplicationController', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedReplicationController - errors', () => {
      it('should have a replaceCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedReplicationController(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedReplicationController', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedReplicationControllerScale - errors', () => {
      it('should have a readCoreV1NamespacedReplicationControllerScale function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedReplicationControllerScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedReplicationControllerScale - errors', () => {
      it('should have a patchCoreV1NamespacedReplicationControllerScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedReplicationControllerScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedReplicationControllerScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedReplicationControllerScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedReplicationControllerScale - errors', () => {
      it('should have a replaceCoreV1NamespacedReplicationControllerScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedReplicationControllerScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedReplicationControllerScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedReplicationControllerScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedReplicationControllerStatus - errors', () => {
      it('should have a readCoreV1NamespacedReplicationControllerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedReplicationControllerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedReplicationControllerStatus - errors', () => {
      it('should have a patchCoreV1NamespacedReplicationControllerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedReplicationControllerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedReplicationControllerStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedReplicationControllerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedReplicationControllerStatus - errors', () => {
      it('should have a replaceCoreV1NamespacedReplicationControllerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedReplicationControllerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedReplicationControllerStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedReplicationControllerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedResourceQuota - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a listCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a createCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedResourceQuota(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedResourceQuota', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a deleteCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a readCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a patchCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedResourceQuota(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedResourceQuota', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a replaceCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedResourceQuota(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedResourceQuota', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedResourceQuotaStatus - errors', () => {
      it('should have a readCoreV1NamespacedResourceQuotaStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedResourceQuotaStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedResourceQuotaStatus - errors', () => {
      it('should have a patchCoreV1NamespacedResourceQuotaStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedResourceQuotaStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedResourceQuotaStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedResourceQuotaStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedResourceQuotaStatus - errors', () => {
      it('should have a replaceCoreV1NamespacedResourceQuotaStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedResourceQuotaStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedResourceQuotaStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedResourceQuotaStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedSecret - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedSecret - errors', () => {
      it('should have a listCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedSecret - errors', () => {
      it('should have a createCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedSecret(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedSecret - errors', () => {
      it('should have a deleteCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedSecret - errors', () => {
      it('should have a readCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedSecret - errors', () => {
      it('should have a patchCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedSecret(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedSecret - errors', () => {
      it('should have a replaceCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedSecret(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedSecret', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNamespacedServiceAccount - errors', () => {
      it('should have a deleteCoreV1CollectionNamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a listCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a createCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedServiceAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedServiceAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a deleteCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a readCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a patchCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedServiceAccount(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedServiceAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a replaceCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedServiceAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedServiceAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1NamespacedService - errors', () => {
      it('should have a listCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1NamespacedService - errors', () => {
      it('should have a createCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1NamespacedService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1NamespacedService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1NamespacedService - errors', () => {
      it('should have a deleteCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedService - errors', () => {
      it('should have a readCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedService - errors', () => {
      it('should have a patchCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedService(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedService - errors', () => {
      it('should have a replaceCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1DeleteNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1GetNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1HeadNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1OptionsNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1PatchNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1PostNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNamespacedServiceProxy - errors', () => {
      it('should have a connectCoreV1PutNamespacedServiceProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNamespacedServiceProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1DeleteNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1GetNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1HeadNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1OptionsNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1PatchNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1PostNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNamespacedServiceProxyWithPath - errors', () => {
      it('should have a connectCoreV1PutNamespacedServiceProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNamespacedServiceProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespacedServiceStatus - errors', () => {
      it('should have a readCoreV1NamespacedServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespacedServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespacedServiceStatus - errors', () => {
      it('should have a patchCoreV1NamespacedServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespacedServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespacedServiceStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespacedServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespacedServiceStatus - errors', () => {
      it('should have a replaceCoreV1NamespacedServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespacedServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespacedServiceStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespacedServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1Namespace - errors', () => {
      it('should have a deleteCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1Namespace - errors', () => {
      it('should have a readCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1Namespace - errors', () => {
      it('should have a patchCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1Namespace(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1Namespace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1Namespace - errors', () => {
      it('should have a replaceCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1Namespace(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1Namespace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespaceFinalize - errors', () => {
      it('should have a replaceCoreV1NamespaceFinalize function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespaceFinalize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespaceFinalize(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespaceFinalize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NamespaceStatus - errors', () => {
      it('should have a readCoreV1NamespaceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NamespaceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NamespaceStatus - errors', () => {
      it('should have a patchCoreV1NamespaceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NamespaceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NamespaceStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NamespaceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NamespaceStatus - errors', () => {
      it('should have a replaceCoreV1NamespaceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NamespaceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NamespaceStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NamespaceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionNode - errors', () => {
      it('should have a deleteCoreV1CollectionNode function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1Node - errors', () => {
      it('should have a listCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1Node - errors', () => {
      it('should have a createCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1Node(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1Node', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1Node - errors', () => {
      it('should have a deleteCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1Node - errors', () => {
      it('should have a readCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1Node - errors', () => {
      it('should have a patchCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1Node(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1Node', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1Node - errors', () => {
      it('should have a replaceCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1Node(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1Node', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNodeProxy - errors', () => {
      it('should have a connectCoreV1DeleteNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNodeProxy - errors', () => {
      it('should have a connectCoreV1GetNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNodeProxy - errors', () => {
      it('should have a connectCoreV1HeadNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNodeProxy - errors', () => {
      it('should have a connectCoreV1OptionsNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNodeProxy - errors', () => {
      it('should have a connectCoreV1PatchNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNodeProxy - errors', () => {
      it('should have a connectCoreV1PostNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNodeProxy - errors', () => {
      it('should have a connectCoreV1PutNodeProxy function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNodeProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1DeleteNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1DeleteNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1DeleteNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1GetNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1GetNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1GetNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1HeadNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1HeadNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1HeadNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1OptionsNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1OptionsNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1OptionsNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PatchNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1PatchNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PatchNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PostNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1PostNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PostNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectCoreV1PutNodeProxyWithPath - errors', () => {
      it('should have a connectCoreV1PutNodeProxyWithPath function', (done) => {
        try {
          assert.equal(true, typeof a.connectCoreV1PutNodeProxyWithPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1NodeStatus - errors', () => {
      it('should have a readCoreV1NodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1NodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1NodeStatus - errors', () => {
      it('should have a patchCoreV1NodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1NodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1NodeStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1NodeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1NodeStatus - errors', () => {
      it('should have a replaceCoreV1NodeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1NodeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1NodeStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1NodeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1PersistentVolumeClaimForAllNamespaces - errors', () => {
      it('should have a listCoreV1PersistentVolumeClaimForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1PersistentVolumeClaimForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1CollectionPersistentVolume - errors', () => {
      it('should have a deleteCoreV1CollectionPersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1CollectionPersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1PersistentVolume - errors', () => {
      it('should have a listCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoreV1PersistentVolume - errors', () => {
      it('should have a createCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.createCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoreV1PersistentVolume(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoreV1PersistentVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoreV1PersistentVolume - errors', () => {
      it('should have a deleteCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1PersistentVolume - errors', () => {
      it('should have a readCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1PersistentVolume - errors', () => {
      it('should have a patchCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1PersistentVolume(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1PersistentVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1PersistentVolume - errors', () => {
      it('should have a replaceCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1PersistentVolume(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1PersistentVolume', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoreV1PersistentVolumeStatus - errors', () => {
      it('should have a readCoreV1PersistentVolumeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCoreV1PersistentVolumeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoreV1PersistentVolumeStatus - errors', () => {
      it('should have a patchCoreV1PersistentVolumeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoreV1PersistentVolumeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoreV1PersistentVolumeStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoreV1PersistentVolumeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoreV1PersistentVolumeStatus - errors', () => {
      it('should have a replaceCoreV1PersistentVolumeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoreV1PersistentVolumeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoreV1PersistentVolumeStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoreV1PersistentVolumeStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1PodForAllNamespaces - errors', () => {
      it('should have a listCoreV1PodForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1PodForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1PodTemplateForAllNamespaces - errors', () => {
      it('should have a listCoreV1PodTemplateForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1PodTemplateForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ReplicationControllerForAllNamespaces - errors', () => {
      it('should have a listCoreV1ReplicationControllerForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ReplicationControllerForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ResourceQuotaForAllNamespaces - errors', () => {
      it('should have a listCoreV1ResourceQuotaForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ResourceQuotaForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1SecretForAllNamespaces - errors', () => {
      it('should have a listCoreV1SecretForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1SecretForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ServiceAccountForAllNamespaces - errors', () => {
      it('should have a listCoreV1ServiceAccountForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ServiceAccountForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoreV1ServiceForAllNamespaces - errors', () => {
      it('should have a listCoreV1ServiceForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoreV1ServiceForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1ConfigMapListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1ConfigMapListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1ConfigMapListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1EndpointsListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1EndpointsListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1EndpointsListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1EventListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1EventListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1EventListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1LimitRangeListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1LimitRangeListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1LimitRangeListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespaceList - errors', () => {
      it('should have a watchCoreV1NamespaceList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespaceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedConfigMapList - errors', () => {
      it('should have a watchCoreV1NamespacedConfigMapList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedConfigMapList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedConfigMap - errors', () => {
      it('should have a watchCoreV1NamespacedConfigMap function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedConfigMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedEndpointsList - errors', () => {
      it('should have a watchCoreV1NamespacedEndpointsList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedEndpointsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedEndpoints - errors', () => {
      it('should have a watchCoreV1NamespacedEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedEventList - errors', () => {
      it('should have a watchCoreV1NamespacedEventList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedEventList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedEvent - errors', () => {
      it('should have a watchCoreV1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedLimitRangeList - errors', () => {
      it('should have a watchCoreV1NamespacedLimitRangeList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedLimitRangeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedLimitRange - errors', () => {
      it('should have a watchCoreV1NamespacedLimitRange function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedLimitRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPersistentVolumeClaimList - errors', () => {
      it('should have a watchCoreV1NamespacedPersistentVolumeClaimList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPersistentVolumeClaimList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPersistentVolumeClaim - errors', () => {
      it('should have a watchCoreV1NamespacedPersistentVolumeClaim function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPersistentVolumeClaim === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPodList - errors', () => {
      it('should have a watchCoreV1NamespacedPodList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPodList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPod - errors', () => {
      it('should have a watchCoreV1NamespacedPod function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPodTemplateList - errors', () => {
      it('should have a watchCoreV1NamespacedPodTemplateList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPodTemplateList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedPodTemplate - errors', () => {
      it('should have a watchCoreV1NamespacedPodTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedPodTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedReplicationControllerList - errors', () => {
      it('should have a watchCoreV1NamespacedReplicationControllerList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedReplicationControllerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedReplicationController - errors', () => {
      it('should have a watchCoreV1NamespacedReplicationController function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedReplicationController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedResourceQuotaList - errors', () => {
      it('should have a watchCoreV1NamespacedResourceQuotaList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedResourceQuotaList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedResourceQuota - errors', () => {
      it('should have a watchCoreV1NamespacedResourceQuota function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedResourceQuota === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedSecretList - errors', () => {
      it('should have a watchCoreV1NamespacedSecretList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedSecretList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedSecret - errors', () => {
      it('should have a watchCoreV1NamespacedSecret function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedSecret === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedServiceAccountList - errors', () => {
      it('should have a watchCoreV1NamespacedServiceAccountList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedServiceAccountList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedServiceAccount - errors', () => {
      it('should have a watchCoreV1NamespacedServiceAccount function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedServiceAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedServiceList - errors', () => {
      it('should have a watchCoreV1NamespacedServiceList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedServiceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NamespacedService - errors', () => {
      it('should have a watchCoreV1NamespacedService function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NamespacedService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1Namespace - errors', () => {
      it('should have a watchCoreV1Namespace function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1Namespace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1NodeList - errors', () => {
      it('should have a watchCoreV1NodeList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1NodeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1Node - errors', () => {
      it('should have a watchCoreV1Node function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1Node === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1PersistentVolumeClaimListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1PersistentVolumeClaimListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1PersistentVolumeClaimListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1PersistentVolumeList - errors', () => {
      it('should have a watchCoreV1PersistentVolumeList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1PersistentVolumeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1PersistentVolume - errors', () => {
      it('should have a watchCoreV1PersistentVolume function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1PersistentVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1PodListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1PodListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1PodListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1PodTemplateListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1PodTemplateListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1PodTemplateListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1ReplicationControllerListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1ReplicationControllerListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1ReplicationControllerListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1ResourceQuotaListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1ResourceQuotaListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1ResourceQuotaListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1SecretListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1SecretListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1SecretListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1ServiceAccountListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1ServiceAccountListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1ServiceAccountListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoreV1ServiceListForAllNamespaces - errors', () => {
      it('should have a watchCoreV1ServiceListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoreV1ServiceListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAPIVersions - errors', () => {
      it('should have a getAPIVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getAPIVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdmissionregistrationAPIGroup - errors', () => {
      it('should have a getAdmissionregistrationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAdmissionregistrationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdmissionregistrationV1beta1APIResources - errors', () => {
      it('should have a getAdmissionregistrationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAdmissionregistrationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdmissionregistrationV1beta1CollectionMutatingWebhookConfiguration - errors', () => {
      it('should have a deleteAdmissionregistrationV1beta1CollectionMutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdmissionregistrationV1beta1CollectionMutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a listAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.listAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a createAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAdmissionregistrationV1beta1MutatingWebhookConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAdmissionregistrationV1beta1MutatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a deleteAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a readAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.readAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a patchAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.patchAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAdmissionregistrationV1beta1MutatingWebhookConfiguration(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAdmissionregistrationV1beta1MutatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdmissionregistrationV1beta1CollectionValidatingWebhookConfiguration - errors', () => {
      it('should have a deleteAdmissionregistrationV1beta1CollectionValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdmissionregistrationV1beta1CollectionValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a listAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.listAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a createAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAdmissionregistrationV1beta1ValidatingWebhookConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAdmissionregistrationV1beta1ValidatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a deleteAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a readAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.readAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAdmissionregistrationV1beta1MutatingWebhookConfigurationList - errors', () => {
      it('should have a watchAdmissionregistrationV1beta1MutatingWebhookConfigurationList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAdmissionregistrationV1beta1MutatingWebhookConfigurationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAdmissionregistrationV1beta1MutatingWebhookConfiguration - errors', () => {
      it('should have a watchAdmissionregistrationV1beta1MutatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.watchAdmissionregistrationV1beta1MutatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAdmissionregistrationV1beta1ValidatingWebhookConfigurationList - errors', () => {
      it('should have a watchAdmissionregistrationV1beta1ValidatingWebhookConfigurationList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAdmissionregistrationV1beta1ValidatingWebhookConfigurationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAdmissionregistrationV1beta1ValidatingWebhookConfiguration - errors', () => {
      it('should have a watchAdmissionregistrationV1beta1ValidatingWebhookConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.watchAdmissionregistrationV1beta1ValidatingWebhookConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiextensionsAPIGroup - errors', () => {
      it('should have a getApiextensionsAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getApiextensionsAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiextensionsV1beta1APIResources - errors', () => {
      it('should have a getApiextensionsV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getApiextensionsV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiextensionsV1beta1CollectionCustomResourceDefinition - errors', () => {
      it('should have a deleteApiextensionsV1beta1CollectionCustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiextensionsV1beta1CollectionCustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a listApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.listApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a createApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.createApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApiextensionsV1beta1CustomResourceDefinition(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createApiextensionsV1beta1CustomResourceDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a deleteApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a readApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.readApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a patchApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiextensionsV1beta1CustomResourceDefinition(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiextensionsV1beta1CustomResourceDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a replaceApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiextensionsV1beta1CustomResourceDefinition(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiextensionsV1beta1CustomResourceDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiextensionsV1beta1CustomResourceDefinitionStatus - errors', () => {
      it('should have a readApiextensionsV1beta1CustomResourceDefinitionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readApiextensionsV1beta1CustomResourceDefinitionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiextensionsV1beta1CustomResourceDefinitionStatus - errors', () => {
      it('should have a patchApiextensionsV1beta1CustomResourceDefinitionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiextensionsV1beta1CustomResourceDefinitionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiextensionsV1beta1CustomResourceDefinitionStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiextensionsV1beta1CustomResourceDefinitionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiextensionsV1beta1CustomResourceDefinitionStatus - errors', () => {
      it('should have a replaceApiextensionsV1beta1CustomResourceDefinitionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiextensionsV1beta1CustomResourceDefinitionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiextensionsV1beta1CustomResourceDefinitionStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiextensionsV1beta1CustomResourceDefinitionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiextensionsV1beta1CustomResourceDefinitionList - errors', () => {
      it('should have a watchApiextensionsV1beta1CustomResourceDefinitionList function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiextensionsV1beta1CustomResourceDefinitionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiextensionsV1beta1CustomResourceDefinition - errors', () => {
      it('should have a watchApiextensionsV1beta1CustomResourceDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiextensionsV1beta1CustomResourceDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiregistrationAPIGroup - errors', () => {
      it('should have a getApiregistrationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getApiregistrationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiregistrationV1APIResources - errors', () => {
      it('should have a getApiregistrationV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getApiregistrationV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiregistrationV1CollectionAPIService - errors', () => {
      it('should have a deleteApiregistrationV1CollectionAPIService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiregistrationV1CollectionAPIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApiregistrationV1APIService - errors', () => {
      it('should have a listApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.listApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApiregistrationV1APIService - errors', () => {
      it('should have a createApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.createApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApiregistrationV1APIService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createApiregistrationV1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiregistrationV1APIService - errors', () => {
      it('should have a deleteApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiregistrationV1APIService - errors', () => {
      it('should have a readApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.readApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiregistrationV1APIService - errors', () => {
      it('should have a patchApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiregistrationV1APIService(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiregistrationV1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiregistrationV1APIService - errors', () => {
      it('should have a replaceApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiregistrationV1APIService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiregistrationV1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiregistrationV1APIServiceStatus - errors', () => {
      it('should have a readApiregistrationV1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readApiregistrationV1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiregistrationV1APIServiceStatus - errors', () => {
      it('should have a patchApiregistrationV1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiregistrationV1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiregistrationV1APIServiceStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiregistrationV1APIServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiregistrationV1APIServiceStatus - errors', () => {
      it('should have a replaceApiregistrationV1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiregistrationV1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiregistrationV1APIServiceStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiregistrationV1APIServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiregistrationV1APIServiceList - errors', () => {
      it('should have a watchApiregistrationV1APIServiceList function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiregistrationV1APIServiceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiregistrationV1APIService - errors', () => {
      it('should have a watchApiregistrationV1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiregistrationV1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiregistrationV1beta1APIResources - errors', () => {
      it('should have a getApiregistrationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getApiregistrationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiregistrationV1beta1CollectionAPIService - errors', () => {
      it('should have a deleteApiregistrationV1beta1CollectionAPIService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiregistrationV1beta1CollectionAPIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApiregistrationV1beta1APIService - errors', () => {
      it('should have a listApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.listApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApiregistrationV1beta1APIService - errors', () => {
      it('should have a createApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.createApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApiregistrationV1beta1APIService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createApiregistrationV1beta1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApiregistrationV1beta1APIService - errors', () => {
      it('should have a deleteApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiregistrationV1beta1APIService - errors', () => {
      it('should have a readApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.readApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiregistrationV1beta1APIService - errors', () => {
      it('should have a patchApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiregistrationV1beta1APIService(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiregistrationV1beta1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiregistrationV1beta1APIService - errors', () => {
      it('should have a replaceApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiregistrationV1beta1APIService(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiregistrationV1beta1APIService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readApiregistrationV1beta1APIServiceStatus - errors', () => {
      it('should have a readApiregistrationV1beta1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readApiregistrationV1beta1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchApiregistrationV1beta1APIServiceStatus - errors', () => {
      it('should have a patchApiregistrationV1beta1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchApiregistrationV1beta1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchApiregistrationV1beta1APIServiceStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchApiregistrationV1beta1APIServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceApiregistrationV1beta1APIServiceStatus - errors', () => {
      it('should have a replaceApiregistrationV1beta1APIServiceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceApiregistrationV1beta1APIServiceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceApiregistrationV1beta1APIServiceStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceApiregistrationV1beta1APIServiceStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiregistrationV1beta1APIServiceList - errors', () => {
      it('should have a watchApiregistrationV1beta1APIServiceList function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiregistrationV1beta1APIServiceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchApiregistrationV1beta1APIService - errors', () => {
      it('should have a watchApiregistrationV1beta1APIService function', (done) => {
        try {
          assert.equal(true, typeof a.watchApiregistrationV1beta1APIService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppsAPIGroup - errors', () => {
      it('should have a getAppsAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAppsAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppsV1APIResources - errors', () => {
      it('should have a getAppsV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAppsV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1ControllerRevisionForAllNamespaces - errors', () => {
      it('should have a listAppsV1ControllerRevisionForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1ControllerRevisionForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1DaemonSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1DaemonSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1DaemonSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1DeploymentForAllNamespaces - errors', () => {
      it('should have a listAppsV1DeploymentForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1DeploymentForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1CollectionNamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1CollectionNamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1CollectionNamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a listAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a createAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a readAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a patchAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedControllerRevision(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a replaceAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1CollectionNamespacedDaemonSet - errors', () => {
      it('should have a deleteAppsV1CollectionNamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1CollectionNamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a listAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a createAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a deleteAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a readAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a patchAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedDaemonSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a replaceAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedDaemonSetStatus - errors', () => {
      it('should have a readAppsV1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedDaemonSetStatus - errors', () => {
      it('should have a patchAppsV1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedDaemonSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedDaemonSetStatus - errors', () => {
      it('should have a replaceAppsV1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedDaemonSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1CollectionNamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1CollectionNamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1CollectionNamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1NamespacedDeployment - errors', () => {
      it('should have a listAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1NamespacedDeployment - errors', () => {
      it('should have a createAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1NamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedDeployment - errors', () => {
      it('should have a readAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedDeployment - errors', () => {
      it('should have a patchAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedDeployment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedDeployment - errors', () => {
      it('should have a replaceAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedDeploymentScale - errors', () => {
      it('should have a readAppsV1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedDeploymentScale - errors', () => {
      it('should have a patchAppsV1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedDeploymentScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedDeploymentScale - errors', () => {
      it('should have a replaceAppsV1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedDeploymentScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedDeploymentStatus - errors', () => {
      it('should have a readAppsV1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedDeploymentStatus - errors', () => {
      it('should have a patchAppsV1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedDeploymentStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedDeploymentStatus - errors', () => {
      it('should have a replaceAppsV1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedDeploymentStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1CollectionNamespacedReplicaSet - errors', () => {
      it('should have a deleteAppsV1CollectionNamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1CollectionNamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a listAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a createAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a deleteAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a readAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a patchAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedReplicaSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a replaceAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedReplicaSetScale - errors', () => {
      it('should have a readAppsV1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedReplicaSetScale - errors', () => {
      it('should have a patchAppsV1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedReplicaSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedReplicaSetScale - errors', () => {
      it('should have a replaceAppsV1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedReplicaSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedReplicaSetStatus - errors', () => {
      it('should have a readAppsV1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedReplicaSetStatus - errors', () => {
      it('should have a patchAppsV1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedReplicaSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedReplicaSetStatus - errors', () => {
      it('should have a replaceAppsV1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedReplicaSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1CollectionNamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1CollectionNamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1CollectionNamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a listAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a createAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a readAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a patchAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedStatefulSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a replaceAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedStatefulSetScale - errors', () => {
      it('should have a readAppsV1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedStatefulSetScale - errors', () => {
      it('should have a patchAppsV1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedStatefulSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedStatefulSetScale - errors', () => {
      it('should have a replaceAppsV1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedStatefulSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1NamespacedStatefulSetStatus - errors', () => {
      it('should have a readAppsV1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1NamespacedStatefulSetStatus - errors', () => {
      it('should have a patchAppsV1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1NamespacedStatefulSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1NamespacedStatefulSetStatus - errors', () => {
      it('should have a replaceAppsV1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1NamespacedStatefulSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1ReplicaSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1ReplicaSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1ReplicaSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1StatefulSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1StatefulSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1StatefulSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1ControllerRevisionListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1ControllerRevisionListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1ControllerRevisionListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1DaemonSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1DaemonSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1DaemonSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1DeploymentListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1DeploymentListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1DeploymentListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedControllerRevisionList - errors', () => {
      it('should have a watchAppsV1NamespacedControllerRevisionList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedControllerRevisionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedControllerRevision - errors', () => {
      it('should have a watchAppsV1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedDaemonSetList - errors', () => {
      it('should have a watchAppsV1NamespacedDaemonSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedDaemonSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedDaemonSet - errors', () => {
      it('should have a watchAppsV1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedDeploymentList - errors', () => {
      it('should have a watchAppsV1NamespacedDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedDeployment - errors', () => {
      it('should have a watchAppsV1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedReplicaSetList - errors', () => {
      it('should have a watchAppsV1NamespacedReplicaSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedReplicaSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedReplicaSet - errors', () => {
      it('should have a watchAppsV1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedStatefulSetList - errors', () => {
      it('should have a watchAppsV1NamespacedStatefulSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedStatefulSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1NamespacedStatefulSet - errors', () => {
      it('should have a watchAppsV1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1ReplicaSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1ReplicaSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1ReplicaSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1StatefulSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1StatefulSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1StatefulSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppsV1beta1APIResources - errors', () => {
      it('should have a getAppsV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAppsV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1ControllerRevisionForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta1ControllerRevisionForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1ControllerRevisionForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1DeploymentForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta1DeploymentForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1DeploymentForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1CollectionNamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1beta1CollectionNamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1CollectionNamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a listAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a createAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta1NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a readAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a patchAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedControllerRevision(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1CollectionNamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1beta1CollectionNamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1CollectionNamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a listAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a createAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a readAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a patchAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedDeployment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta1NamespacedDeploymentRollback - errors', () => {
      it('should have a createAppsV1beta1NamespacedDeploymentRollback function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta1NamespacedDeploymentRollback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta1NamespacedDeploymentRollback(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta1NamespacedDeploymentRollback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a readAppsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a patchAppsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedDeploymentScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedDeploymentScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a readAppsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a patchAppsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedDeploymentStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedDeploymentStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1CollectionNamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1beta1CollectionNamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1CollectionNamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a listAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a createAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta1NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a readAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a patchAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedStatefulSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedStatefulSetScale - errors', () => {
      it('should have a readAppsV1beta1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedStatefulSetScale - errors', () => {
      it('should have a patchAppsV1beta1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedStatefulSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedStatefulSetScale - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedStatefulSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta1NamespacedStatefulSetStatus - errors', () => {
      it('should have a readAppsV1beta1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta1NamespacedStatefulSetStatus - errors', () => {
      it('should have a patchAppsV1beta1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta1NamespacedStatefulSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta1NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta1NamespacedStatefulSetStatus - errors', () => {
      it('should have a replaceAppsV1beta1NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta1NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta1NamespacedStatefulSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta1NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta1StatefulSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta1StatefulSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta1StatefulSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1ControllerRevisionListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta1ControllerRevisionListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1ControllerRevisionListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1DeploymentListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta1DeploymentListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1DeploymentListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedControllerRevisionList - errors', () => {
      it('should have a watchAppsV1beta1NamespacedControllerRevisionList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedControllerRevisionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedControllerRevision - errors', () => {
      it('should have a watchAppsV1beta1NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedDeploymentList - errors', () => {
      it('should have a watchAppsV1beta1NamespacedDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedDeployment - errors', () => {
      it('should have a watchAppsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedStatefulSetList - errors', () => {
      it('should have a watchAppsV1beta1NamespacedStatefulSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedStatefulSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1NamespacedStatefulSet - errors', () => {
      it('should have a watchAppsV1beta1NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta1StatefulSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta1StatefulSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta1StatefulSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppsV1beta2APIResources - errors', () => {
      it('should have a getAppsV1beta2APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAppsV1beta2APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2ControllerRevisionForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta2ControllerRevisionForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2ControllerRevisionForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2DaemonSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta2DaemonSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2DaemonSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2DeploymentForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta2DeploymentForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2DeploymentForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2CollectionNamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1beta2CollectionNamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2CollectionNamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a listAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a createAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta2NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta2NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a deleteAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a readAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a patchAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedControllerRevision(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedControllerRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedControllerRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2CollectionNamespacedDaemonSet - errors', () => {
      it('should have a deleteAppsV1beta2CollectionNamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2CollectionNamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a listAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a createAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta2NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta2NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a deleteAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a readAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a patchAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedDaemonSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedDaemonSetStatus - errors', () => {
      it('should have a readAppsV1beta2NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedDaemonSetStatus - errors', () => {
      it('should have a patchAppsV1beta2NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedDaemonSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedDaemonSetStatus - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedDaemonSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2CollectionNamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1beta2CollectionNamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2CollectionNamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a listAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a createAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta2NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta2NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a deleteAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a readAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a patchAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedDeployment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedDeploymentScale - errors', () => {
      it('should have a readAppsV1beta2NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedDeploymentScale - errors', () => {
      it('should have a patchAppsV1beta2NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedDeploymentScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedDeploymentScale - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedDeploymentScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedDeploymentStatus - errors', () => {
      it('should have a readAppsV1beta2NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedDeploymentStatus - errors', () => {
      it('should have a patchAppsV1beta2NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedDeploymentStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedDeploymentStatus - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedDeploymentStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2CollectionNamespacedReplicaSet - errors', () => {
      it('should have a deleteAppsV1beta2CollectionNamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2CollectionNamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a listAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a createAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta2NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta2NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a deleteAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a readAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a patchAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedReplicaSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedReplicaSetScale - errors', () => {
      it('should have a readAppsV1beta2NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedReplicaSetScale - errors', () => {
      it('should have a patchAppsV1beta2NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedReplicaSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedReplicaSetScale - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedReplicaSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedReplicaSetStatus - errors', () => {
      it('should have a readAppsV1beta2NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedReplicaSetStatus - errors', () => {
      it('should have a patchAppsV1beta2NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedReplicaSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedReplicaSetStatus - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedReplicaSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2CollectionNamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1beta2CollectionNamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2CollectionNamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a listAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a createAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.createAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAppsV1beta2NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAppsV1beta2NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a deleteAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a readAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a patchAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedStatefulSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedStatefulSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedStatefulSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedStatefulSetScale - errors', () => {
      it('should have a readAppsV1beta2NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedStatefulSetScale - errors', () => {
      it('should have a patchAppsV1beta2NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedStatefulSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedStatefulSetScale - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedStatefulSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedStatefulSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedStatefulSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedStatefulSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAppsV1beta2NamespacedStatefulSetStatus - errors', () => {
      it('should have a readAppsV1beta2NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAppsV1beta2NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAppsV1beta2NamespacedStatefulSetStatus - errors', () => {
      it('should have a patchAppsV1beta2NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAppsV1beta2NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAppsV1beta2NamespacedStatefulSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAppsV1beta2NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAppsV1beta2NamespacedStatefulSetStatus - errors', () => {
      it('should have a replaceAppsV1beta2NamespacedStatefulSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAppsV1beta2NamespacedStatefulSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAppsV1beta2NamespacedStatefulSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAppsV1beta2NamespacedStatefulSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2ReplicaSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta2ReplicaSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2ReplicaSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAppsV1beta2StatefulSetForAllNamespaces - errors', () => {
      it('should have a listAppsV1beta2StatefulSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAppsV1beta2StatefulSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2ControllerRevisionListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta2ControllerRevisionListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2ControllerRevisionListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2DaemonSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta2DaemonSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2DaemonSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2DeploymentListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta2DeploymentListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2DeploymentListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedControllerRevisionList - errors', () => {
      it('should have a watchAppsV1beta2NamespacedControllerRevisionList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedControllerRevisionList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedControllerRevision - errors', () => {
      it('should have a watchAppsV1beta2NamespacedControllerRevision function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedControllerRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedDaemonSetList - errors', () => {
      it('should have a watchAppsV1beta2NamespacedDaemonSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedDaemonSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedDaemonSet - errors', () => {
      it('should have a watchAppsV1beta2NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedDeploymentList - errors', () => {
      it('should have a watchAppsV1beta2NamespacedDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedDeployment - errors', () => {
      it('should have a watchAppsV1beta2NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedReplicaSetList - errors', () => {
      it('should have a watchAppsV1beta2NamespacedReplicaSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedReplicaSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedReplicaSet - errors', () => {
      it('should have a watchAppsV1beta2NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedStatefulSetList - errors', () => {
      it('should have a watchAppsV1beta2NamespacedStatefulSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedStatefulSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2NamespacedStatefulSet - errors', () => {
      it('should have a watchAppsV1beta2NamespacedStatefulSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2NamespacedStatefulSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2ReplicaSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta2ReplicaSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2ReplicaSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAppsV1beta2StatefulSetListForAllNamespaces - errors', () => {
      it('should have a watchAppsV1beta2StatefulSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAppsV1beta2StatefulSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditregistrationAPIGroup - errors', () => {
      it('should have a getAuditregistrationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditregistrationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuditregistrationV1alpha1APIResources - errors', () => {
      it('should have a getAuditregistrationV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAuditregistrationV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuditregistrationV1alpha1CollectionAuditSink - errors', () => {
      it('should have a deleteAuditregistrationV1alpha1CollectionAuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuditregistrationV1alpha1CollectionAuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a listAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.listAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a createAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.createAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuditregistrationV1alpha1AuditSink(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuditregistrationV1alpha1AuditSink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a deleteAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a readAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.readAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a patchAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.patchAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAuditregistrationV1alpha1AuditSink(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAuditregistrationV1alpha1AuditSink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a replaceAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAuditregistrationV1alpha1AuditSink(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAuditregistrationV1alpha1AuditSink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAuditregistrationV1alpha1AuditSinkList - errors', () => {
      it('should have a watchAuditregistrationV1alpha1AuditSinkList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAuditregistrationV1alpha1AuditSinkList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAuditregistrationV1alpha1AuditSink - errors', () => {
      it('should have a watchAuditregistrationV1alpha1AuditSink function', (done) => {
        try {
          assert.equal(true, typeof a.watchAuditregistrationV1alpha1AuditSink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationAPIGroup - errors', () => {
      it('should have a getAuthenticationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationV1APIResources - errors', () => {
      it('should have a getAuthenticationV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthenticationV1TokenReview - errors', () => {
      it('should have a createAuthenticationV1TokenReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthenticationV1TokenReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthenticationV1TokenReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthenticationV1TokenReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationV1beta1APIResources - errors', () => {
      it('should have a getAuthenticationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthenticationV1beta1TokenReview - errors', () => {
      it('should have a createAuthenticationV1beta1TokenReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthenticationV1beta1TokenReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthenticationV1beta1TokenReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthenticationV1beta1TokenReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationAPIGroup - errors', () => {
      it('should have a getAuthorizationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationV1APIResources - errors', () => {
      it('should have a getAuthorizationV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1NamespacedLocalSubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1NamespacedLocalSubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1NamespacedLocalSubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1NamespacedLocalSubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1NamespacedLocalSubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1SelfSubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1SelfSubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1SelfSubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1SelfSubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1SelfSubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1SelfSubjectRulesReview - errors', () => {
      it('should have a createAuthorizationV1SelfSubjectRulesReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1SelfSubjectRulesReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1SelfSubjectRulesReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1SelfSubjectRulesReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1SubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1SubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1SubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1SubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1SubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthorizationV1beta1APIResources - errors', () => {
      it('should have a getAuthorizationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthorizationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1beta1NamespacedLocalSubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1beta1NamespacedLocalSubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1beta1NamespacedLocalSubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1beta1NamespacedLocalSubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1beta1NamespacedLocalSubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1beta1SelfSubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1beta1SelfSubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1beta1SelfSubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1beta1SelfSubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1beta1SelfSubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1beta1SelfSubjectRulesReview - errors', () => {
      it('should have a createAuthorizationV1beta1SelfSubjectRulesReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1beta1SelfSubjectRulesReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1beta1SelfSubjectRulesReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1beta1SelfSubjectRulesReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthorizationV1beta1SubjectAccessReview - errors', () => {
      it('should have a createAuthorizationV1beta1SubjectAccessReview function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthorizationV1beta1SubjectAccessReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAuthorizationV1beta1SubjectAccessReview(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAuthorizationV1beta1SubjectAccessReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutoscalingAPIGroup - errors', () => {
      it('should have a getAutoscalingAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAutoscalingAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutoscalingV1APIResources - errors', () => {
      it('should have a getAutoscalingV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAutoscalingV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV1HorizontalPodAutoscalerForAllNamespaces - errors', () => {
      it('should have a listAutoscalingV1HorizontalPodAutoscalerForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV1HorizontalPodAutoscalerForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV1CollectionNamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV1CollectionNamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV1CollectionNamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a listAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a createAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.createAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAutoscalingV1NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAutoscalingV1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a readAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a patchAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV1NamespacedHorizontalPodAutoscaler(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a replaceAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV1NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a readAutoscalingV1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV1HorizontalPodAutoscalerListForAllNamespaces - errors', () => {
      it('should have a watchAutoscalingV1HorizontalPodAutoscalerListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV1HorizontalPodAutoscalerListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV1NamespacedHorizontalPodAutoscalerList - errors', () => {
      it('should have a watchAutoscalingV1NamespacedHorizontalPodAutoscalerList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV1NamespacedHorizontalPodAutoscalerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a watchAutoscalingV1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutoscalingV2beta1APIResources - errors', () => {
      it('should have a getAutoscalingV2beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAutoscalingV2beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV2beta1HorizontalPodAutoscalerForAllNamespaces - errors', () => {
      it('should have a listAutoscalingV2beta1HorizontalPodAutoscalerForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV2beta1HorizontalPodAutoscalerForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV2beta1CollectionNamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV2beta1CollectionNamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV2beta1CollectionNamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a listAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a readAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a readAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta1HorizontalPodAutoscalerListForAllNamespaces - errors', () => {
      it('should have a watchAutoscalingV2beta1HorizontalPodAutoscalerListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta1HorizontalPodAutoscalerListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerList - errors', () => {
      it('should have a watchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a watchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutoscalingV2beta2APIResources - errors', () => {
      it('should have a getAutoscalingV2beta2APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getAutoscalingV2beta2APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV2beta2HorizontalPodAutoscalerForAllNamespaces - errors', () => {
      it('should have a listAutoscalingV2beta2HorizontalPodAutoscalerForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV2beta2HorizontalPodAutoscalerForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV2beta2CollectionNamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV2beta2CollectionNamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV2beta2CollectionNamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a listAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.listAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a deleteAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a readAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a readAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus - errors', () => {
      it('should have a replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta2HorizontalPodAutoscalerListForAllNamespaces - errors', () => {
      it('should have a watchAutoscalingV2beta2HorizontalPodAutoscalerListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta2HorizontalPodAutoscalerListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerList - errors', () => {
      it('should have a watchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerList function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler - errors', () => {
      it('should have a watchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler function', (done) => {
        try {
          assert.equal(true, typeof a.watchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchAPIGroup - errors', () => {
      it('should have a getBatchAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getBatchAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchV1APIResources - errors', () => {
      it('should have a getBatchV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getBatchV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV1JobForAllNamespaces - errors', () => {
      it('should have a listBatchV1JobForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV1JobForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV1CollectionNamespacedJob - errors', () => {
      it('should have a deleteBatchV1CollectionNamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV1CollectionNamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV1NamespacedJob - errors', () => {
      it('should have a listBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBatchV1NamespacedJob - errors', () => {
      it('should have a createBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.createBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBatchV1NamespacedJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createBatchV1NamespacedJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV1NamespacedJob - errors', () => {
      it('should have a deleteBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV1NamespacedJob - errors', () => {
      it('should have a readBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV1NamespacedJob - errors', () => {
      it('should have a patchBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV1NamespacedJob(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV1NamespacedJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV1NamespacedJob - errors', () => {
      it('should have a replaceBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV1NamespacedJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV1NamespacedJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV1NamespacedJobStatus - errors', () => {
      it('should have a readBatchV1NamespacedJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV1NamespacedJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV1NamespacedJobStatus - errors', () => {
      it('should have a patchBatchV1NamespacedJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV1NamespacedJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV1NamespacedJobStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV1NamespacedJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV1NamespacedJobStatus - errors', () => {
      it('should have a replaceBatchV1NamespacedJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV1NamespacedJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV1NamespacedJobStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV1NamespacedJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1JobListForAllNamespaces - errors', () => {
      it('should have a watchBatchV1JobListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1JobListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1NamespacedJobList - errors', () => {
      it('should have a watchBatchV1NamespacedJobList function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1NamespacedJobList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1NamespacedJob - errors', () => {
      it('should have a watchBatchV1NamespacedJob function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1NamespacedJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchV1beta1APIResources - errors', () => {
      it('should have a getBatchV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getBatchV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV1beta1CronJobForAllNamespaces - errors', () => {
      it('should have a listBatchV1beta1CronJobForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV1beta1CronJobForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV1beta1CollectionNamespacedCronJob - errors', () => {
      it('should have a deleteBatchV1beta1CollectionNamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV1beta1CollectionNamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a listBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a createBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.createBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBatchV1beta1NamespacedCronJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createBatchV1beta1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a deleteBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a readBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a patchBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV1beta1NamespacedCronJob(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV1beta1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a replaceBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV1beta1NamespacedCronJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV1beta1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV1beta1NamespacedCronJobStatus - errors', () => {
      it('should have a readBatchV1beta1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV1beta1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV1beta1NamespacedCronJobStatus - errors', () => {
      it('should have a patchBatchV1beta1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV1beta1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV1beta1NamespacedCronJobStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV1beta1NamespacedCronJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV1beta1NamespacedCronJobStatus - errors', () => {
      it('should have a replaceBatchV1beta1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV1beta1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV1beta1NamespacedCronJobStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV1beta1NamespacedCronJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1beta1CronJobListForAllNamespaces - errors', () => {
      it('should have a watchBatchV1beta1CronJobListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1beta1CronJobListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1beta1NamespacedCronJobList - errors', () => {
      it('should have a watchBatchV1beta1NamespacedCronJobList function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1beta1NamespacedCronJobList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV1beta1NamespacedCronJob - errors', () => {
      it('should have a watchBatchV1beta1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV1beta1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBatchV2alpha1APIResources - errors', () => {
      it('should have a getBatchV2alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getBatchV2alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV2alpha1CronJobForAllNamespaces - errors', () => {
      it('should have a listBatchV2alpha1CronJobForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV2alpha1CronJobForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV2alpha1CollectionNamespacedCronJob - errors', () => {
      it('should have a deleteBatchV2alpha1CollectionNamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV2alpha1CollectionNamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a listBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.listBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a createBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.createBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBatchV2alpha1NamespacedCronJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createBatchV2alpha1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a deleteBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a readBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a patchBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV2alpha1NamespacedCronJob(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV2alpha1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a replaceBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV2alpha1NamespacedCronJob(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV2alpha1NamespacedCronJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readBatchV2alpha1NamespacedCronJobStatus - errors', () => {
      it('should have a readBatchV2alpha1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readBatchV2alpha1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBatchV2alpha1NamespacedCronJobStatus - errors', () => {
      it('should have a patchBatchV2alpha1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchBatchV2alpha1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBatchV2alpha1NamespacedCronJobStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchBatchV2alpha1NamespacedCronJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceBatchV2alpha1NamespacedCronJobStatus - errors', () => {
      it('should have a replaceBatchV2alpha1NamespacedCronJobStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceBatchV2alpha1NamespacedCronJobStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceBatchV2alpha1NamespacedCronJobStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceBatchV2alpha1NamespacedCronJobStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV2alpha1CronJobListForAllNamespaces - errors', () => {
      it('should have a watchBatchV2alpha1CronJobListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV2alpha1CronJobListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV2alpha1NamespacedCronJobList - errors', () => {
      it('should have a watchBatchV2alpha1NamespacedCronJobList function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV2alpha1NamespacedCronJobList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchBatchV2alpha1NamespacedCronJob - errors', () => {
      it('should have a watchBatchV2alpha1NamespacedCronJob function', (done) => {
        try {
          assert.equal(true, typeof a.watchBatchV2alpha1NamespacedCronJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificatesAPIGroup - errors', () => {
      it('should have a getCertificatesAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificatesAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificatesV1beta1APIResources - errors', () => {
      it('should have a getCertificatesV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificatesV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificatesV1beta1CollectionCertificateSigningRequest - errors', () => {
      it('should have a deleteCertificatesV1beta1CollectionCertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertificatesV1beta1CollectionCertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a listCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.listCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a createCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCertificatesV1beta1CertificateSigningRequest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCertificatesV1beta1CertificateSigningRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a deleteCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a readCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.readCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a patchCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.patchCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCertificatesV1beta1CertificateSigningRequest(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCertificatesV1beta1CertificateSigningRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a replaceCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCertificatesV1beta1CertificateSigningRequest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCertificatesV1beta1CertificateSigningRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCertificatesV1beta1CertificateSigningRequestApproval - errors', () => {
      it('should have a replaceCertificatesV1beta1CertificateSigningRequestApproval function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCertificatesV1beta1CertificateSigningRequestApproval === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCertificatesV1beta1CertificateSigningRequestApproval(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCertificatesV1beta1CertificateSigningRequestApproval', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCertificatesV1beta1CertificateSigningRequestStatus - errors', () => {
      it('should have a readCertificatesV1beta1CertificateSigningRequestStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readCertificatesV1beta1CertificateSigningRequestStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCertificatesV1beta1CertificateSigningRequestStatus - errors', () => {
      it('should have a patchCertificatesV1beta1CertificateSigningRequestStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchCertificatesV1beta1CertificateSigningRequestStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCertificatesV1beta1CertificateSigningRequestStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCertificatesV1beta1CertificateSigningRequestStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCertificatesV1beta1CertificateSigningRequestStatus - errors', () => {
      it('should have a replaceCertificatesV1beta1CertificateSigningRequestStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCertificatesV1beta1CertificateSigningRequestStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCertificatesV1beta1CertificateSigningRequestStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCertificatesV1beta1CertificateSigningRequestStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCertificatesV1beta1CertificateSigningRequestList - errors', () => {
      it('should have a watchCertificatesV1beta1CertificateSigningRequestList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCertificatesV1beta1CertificateSigningRequestList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCertificatesV1beta1CertificateSigningRequest - errors', () => {
      it('should have a watchCertificatesV1beta1CertificateSigningRequest function', (done) => {
        try {
          assert.equal(true, typeof a.watchCertificatesV1beta1CertificateSigningRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCoordinationAPIGroup - errors', () => {
      it('should have a getCoordinationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getCoordinationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCoordinationV1APIResources - errors', () => {
      it('should have a getCoordinationV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getCoordinationV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoordinationV1LeaseForAllNamespaces - errors', () => {
      it('should have a listCoordinationV1LeaseForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoordinationV1LeaseForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoordinationV1CollectionNamespacedLease - errors', () => {
      it('should have a deleteCoordinationV1CollectionNamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoordinationV1CollectionNamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoordinationV1NamespacedLease - errors', () => {
      it('should have a listCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.listCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoordinationV1NamespacedLease - errors', () => {
      it('should have a createCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.createCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoordinationV1NamespacedLease(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoordinationV1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoordinationV1NamespacedLease - errors', () => {
      it('should have a deleteCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoordinationV1NamespacedLease - errors', () => {
      it('should have a readCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.readCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoordinationV1NamespacedLease - errors', () => {
      it('should have a patchCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoordinationV1NamespacedLease(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoordinationV1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoordinationV1NamespacedLease - errors', () => {
      it('should have a replaceCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoordinationV1NamespacedLease(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoordinationV1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1LeaseListForAllNamespaces - errors', () => {
      it('should have a watchCoordinationV1LeaseListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1LeaseListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1NamespacedLeaseList - errors', () => {
      it('should have a watchCoordinationV1NamespacedLeaseList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1NamespacedLeaseList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1NamespacedLease - errors', () => {
      it('should have a watchCoordinationV1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCoordinationV1beta1APIResources - errors', () => {
      it('should have a getCoordinationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getCoordinationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoordinationV1beta1LeaseForAllNamespaces - errors', () => {
      it('should have a listCoordinationV1beta1LeaseForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listCoordinationV1beta1LeaseForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoordinationV1beta1CollectionNamespacedLease - errors', () => {
      it('should have a deleteCoordinationV1beta1CollectionNamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoordinationV1beta1CollectionNamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a listCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.listCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a createCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.createCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCoordinationV1beta1NamespacedLease(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createCoordinationV1beta1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a deleteCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a readCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.readCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a patchCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.patchCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCoordinationV1beta1NamespacedLease(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchCoordinationV1beta1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a replaceCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceCoordinationV1beta1NamespacedLease(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceCoordinationV1beta1NamespacedLease', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1beta1LeaseListForAllNamespaces - errors', () => {
      it('should have a watchCoordinationV1beta1LeaseListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1beta1LeaseListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1beta1NamespacedLeaseList - errors', () => {
      it('should have a watchCoordinationV1beta1NamespacedLeaseList function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1beta1NamespacedLeaseList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchCoordinationV1beta1NamespacedLease - errors', () => {
      it('should have a watchCoordinationV1beta1NamespacedLease function', (done) => {
        try {
          assert.equal(true, typeof a.watchCoordinationV1beta1NamespacedLease === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventsAPIGroup - errors', () => {
      it('should have a getEventsAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getEventsAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventsV1beta1APIResources - errors', () => {
      it('should have a getEventsV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getEventsV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEventsV1beta1EventForAllNamespaces - errors', () => {
      it('should have a listEventsV1beta1EventForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listEventsV1beta1EventForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventsV1beta1CollectionNamespacedEvent - errors', () => {
      it('should have a deleteEventsV1beta1CollectionNamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEventsV1beta1CollectionNamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a listEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.listEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a createEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.createEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createEventsV1beta1NamespacedEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createEventsV1beta1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a deleteEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a readEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.readEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a patchEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.patchEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchEventsV1beta1NamespacedEvent(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchEventsV1beta1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a replaceEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.replaceEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceEventsV1beta1NamespacedEvent(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceEventsV1beta1NamespacedEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchEventsV1beta1EventListForAllNamespaces - errors', () => {
      it('should have a watchEventsV1beta1EventListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchEventsV1beta1EventListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchEventsV1beta1NamespacedEventList - errors', () => {
      it('should have a watchEventsV1beta1NamespacedEventList function', (done) => {
        try {
          assert.equal(true, typeof a.watchEventsV1beta1NamespacedEventList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchEventsV1beta1NamespacedEvent - errors', () => {
      it('should have a watchEventsV1beta1NamespacedEvent function', (done) => {
        try {
          assert.equal(true, typeof a.watchEventsV1beta1NamespacedEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensionsAPIGroup - errors', () => {
      it('should have a getExtensionsAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensionsAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensionsV1beta1APIResources - errors', () => {
      it('should have a getExtensionsV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensionsV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1DaemonSetForAllNamespaces - errors', () => {
      it('should have a listExtensionsV1beta1DaemonSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1DaemonSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1DeploymentForAllNamespaces - errors', () => {
      it('should have a listExtensionsV1beta1DeploymentForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1DeploymentForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1IngressForAllNamespaces - errors', () => {
      it('should have a listExtensionsV1beta1IngressForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1IngressForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionNamespacedDaemonSet - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionNamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionNamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a listExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a deleteExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedDaemonSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedDaemonSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedDaemonSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedDaemonSetStatus - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedDaemonSetStatus - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedDaemonSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedDaemonSetStatus - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedDaemonSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedDaemonSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedDaemonSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedDaemonSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionNamespacedDeployment - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionNamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionNamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a listExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a deleteExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedDeployment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedDeployment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedDeploymentRollback - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedDeploymentRollback function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedDeploymentRollback === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedDeploymentRollback(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedDeploymentRollback', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedDeploymentScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedDeploymentScale - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedDeploymentScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedDeploymentScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedDeploymentScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedDeploymentScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedDeploymentStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedDeploymentStatus - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedDeploymentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedDeploymentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedDeploymentStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedDeploymentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionNamespacedIngress - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionNamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionNamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a listExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedIngress(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a deleteExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedIngress(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedIngress(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedIngressStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedIngressStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedIngressStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedIngressStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionNamespacedNetworkPolicy - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionNamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionNamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a listExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedNetworkPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a deleteExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedNetworkPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedNetworkPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionNamespacedReplicaSet - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionNamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionNamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a listExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a createExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a deleteExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedReplicaSet(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedReplicaSet(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedReplicaSet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedReplicaSetScale - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedReplicaSetScale - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedReplicaSetScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedReplicaSetScale - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedReplicaSetScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedReplicaSetScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedReplicaSetScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedReplicaSetScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedReplicaSetStatus - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedReplicaSetStatus - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedReplicaSetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedReplicaSetStatus - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedReplicaSetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedReplicaSetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedReplicaSetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedReplicaSetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1NamespacedReplicationControllerDummyScale - errors', () => {
      it('should have a readExtensionsV1beta1NamespacedReplicationControllerDummyScale function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1NamespacedReplicationControllerDummyScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1NamespacedReplicationControllerDummyScale - errors', () => {
      it('should have a patchExtensionsV1beta1NamespacedReplicationControllerDummyScale function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1NamespacedReplicationControllerDummyScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1NamespacedReplicationControllerDummyScale(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1NamespacedReplicationControllerDummyScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale - errors', () => {
      it('should have a replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1NetworkPolicyForAllNamespaces - errors', () => {
      it('should have a listExtensionsV1beta1NetworkPolicyForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1NetworkPolicyForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1CollectionPodSecurityPolicy - errors', () => {
      it('should have a deleteExtensionsV1beta1CollectionPodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1CollectionPodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a listExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a createExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensionsV1beta1PodSecurityPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createExtensionsV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a deleteExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a readExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.readExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a patchExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchExtensionsV1beta1PodSecurityPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchExtensionsV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a replaceExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.replaceExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceExtensionsV1beta1PodSecurityPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceExtensionsV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensionsV1beta1ReplicaSetForAllNamespaces - errors', () => {
      it('should have a listExtensionsV1beta1ReplicaSetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensionsV1beta1ReplicaSetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1DaemonSetListForAllNamespaces - errors', () => {
      it('should have a watchExtensionsV1beta1DaemonSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1DaemonSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1DeploymentListForAllNamespaces - errors', () => {
      it('should have a watchExtensionsV1beta1DeploymentListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1DeploymentListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1IngressListForAllNamespaces - errors', () => {
      it('should have a watchExtensionsV1beta1IngressListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1IngressListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedDaemonSetList - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedDaemonSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedDaemonSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedDaemonSet - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedDaemonSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedDaemonSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedDeploymentList - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedDeploymentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedDeploymentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedDeployment - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedIngressList - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedIngressList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedIngressList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedIngress - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedNetworkPolicyList - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedNetworkPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedNetworkPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedNetworkPolicy - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedReplicaSetList - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedReplicaSetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedReplicaSetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NamespacedReplicaSet - errors', () => {
      it('should have a watchExtensionsV1beta1NamespacedReplicaSet function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NamespacedReplicaSet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1NetworkPolicyListForAllNamespaces - errors', () => {
      it('should have a watchExtensionsV1beta1NetworkPolicyListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1NetworkPolicyListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1PodSecurityPolicyList - errors', () => {
      it('should have a watchExtensionsV1beta1PodSecurityPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1PodSecurityPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1PodSecurityPolicy - errors', () => {
      it('should have a watchExtensionsV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchExtensionsV1beta1ReplicaSetListForAllNamespaces - errors', () => {
      it('should have a watchExtensionsV1beta1ReplicaSetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchExtensionsV1beta1ReplicaSetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkingAPIGroup - errors', () => {
      it('should have a getNetworkingAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkingAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkingV1APIResources - errors', () => {
      it('should have a getNetworkingV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkingV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkingV1CollectionNamespacedNetworkPolicy - errors', () => {
      it('should have a deleteNetworkingV1CollectionNamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkingV1CollectionNamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a listNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a createNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkingV1NamespacedNetworkPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createNetworkingV1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a deleteNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a readNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.readNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a patchNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchNetworkingV1NamespacedNetworkPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchNetworkingV1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a replaceNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceNetworkingV1NamespacedNetworkPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceNetworkingV1NamespacedNetworkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkingV1NetworkPolicyForAllNamespaces - errors', () => {
      it('should have a listNetworkingV1NetworkPolicyForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkingV1NetworkPolicyForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1NamespacedNetworkPolicyList - errors', () => {
      it('should have a watchNetworkingV1NamespacedNetworkPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1NamespacedNetworkPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1NamespacedNetworkPolicy - errors', () => {
      it('should have a watchNetworkingV1NamespacedNetworkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1NamespacedNetworkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1NetworkPolicyListForAllNamespaces - errors', () => {
      it('should have a watchNetworkingV1NetworkPolicyListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1NetworkPolicyListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkingV1beta1APIResources - errors', () => {
      it('should have a getNetworkingV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkingV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkingV1beta1IngressForAllNamespaces - errors', () => {
      it('should have a listNetworkingV1beta1IngressForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkingV1beta1IngressForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkingV1beta1CollectionNamespacedIngress - errors', () => {
      it('should have a deleteNetworkingV1beta1CollectionNamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkingV1beta1CollectionNamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a listNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a createNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkingV1beta1NamespacedIngress(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createNetworkingV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a deleteNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a readNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.readNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a patchNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.patchNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchNetworkingV1beta1NamespacedIngress(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchNetworkingV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a replaceNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceNetworkingV1beta1NamespacedIngress(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceNetworkingV1beta1NamespacedIngress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readNetworkingV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a readNetworkingV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readNetworkingV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNetworkingV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a patchNetworkingV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchNetworkingV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchNetworkingV1beta1NamespacedIngressStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchNetworkingV1beta1NamespacedIngressStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNetworkingV1beta1NamespacedIngressStatus - errors', () => {
      it('should have a replaceNetworkingV1beta1NamespacedIngressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNetworkingV1beta1NamespacedIngressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceNetworkingV1beta1NamespacedIngressStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceNetworkingV1beta1NamespacedIngressStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1beta1IngressListForAllNamespaces - errors', () => {
      it('should have a watchNetworkingV1beta1IngressListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1beta1IngressListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1beta1NamespacedIngressList - errors', () => {
      it('should have a watchNetworkingV1beta1NamespacedIngressList function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1beta1NamespacedIngressList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNetworkingV1beta1NamespacedIngress - errors', () => {
      it('should have a watchNetworkingV1beta1NamespacedIngress function', (done) => {
        try {
          assert.equal(true, typeof a.watchNetworkingV1beta1NamespacedIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeAPIGroup - errors', () => {
      it('should have a getNodeAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeV1alpha1APIResources - errors', () => {
      it('should have a getNodeV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeV1alpha1CollectionRuntimeClass - errors', () => {
      it('should have a deleteNodeV1alpha1CollectionRuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeV1alpha1CollectionRuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a listNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.listNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a createNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.createNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNodeV1alpha1RuntimeClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createNodeV1alpha1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a deleteNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a readNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.readNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a patchNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchNodeV1alpha1RuntimeClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchNodeV1alpha1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a replaceNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceNodeV1alpha1RuntimeClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceNodeV1alpha1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNodeV1alpha1RuntimeClassList - errors', () => {
      it('should have a watchNodeV1alpha1RuntimeClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchNodeV1alpha1RuntimeClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNodeV1alpha1RuntimeClass - errors', () => {
      it('should have a watchNodeV1alpha1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchNodeV1alpha1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeV1beta1APIResources - errors', () => {
      it('should have a getNodeV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getNodeV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeV1beta1CollectionRuntimeClass - errors', () => {
      it('should have a deleteNodeV1beta1CollectionRuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeV1beta1CollectionRuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNodeV1beta1RuntimeClass - errors', () => {
      it('should have a listNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.listNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNodeV1beta1RuntimeClass - errors', () => {
      it('should have a createNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.createNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNodeV1beta1RuntimeClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createNodeV1beta1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeV1beta1RuntimeClass - errors', () => {
      it('should have a deleteNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readNodeV1beta1RuntimeClass - errors', () => {
      it('should have a readNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.readNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNodeV1beta1RuntimeClass - errors', () => {
      it('should have a patchNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchNodeV1beta1RuntimeClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchNodeV1beta1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNodeV1beta1RuntimeClass - errors', () => {
      it('should have a replaceNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceNodeV1beta1RuntimeClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceNodeV1beta1RuntimeClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNodeV1beta1RuntimeClassList - errors', () => {
      it('should have a watchNodeV1beta1RuntimeClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchNodeV1beta1RuntimeClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchNodeV1beta1RuntimeClass - errors', () => {
      it('should have a watchNodeV1beta1RuntimeClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchNodeV1beta1RuntimeClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyAPIGroup - errors', () => {
      it('should have a getPolicyAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyV1beta1APIResources - errors', () => {
      it('should have a getPolicyV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyV1beta1CollectionNamespacedPodDisruptionBudget - errors', () => {
      it('should have a deletePolicyV1beta1CollectionNamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyV1beta1CollectionNamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a listPolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a createPolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicyV1beta1NamespacedPodDisruptionBudget(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createPolicyV1beta1NamespacedPodDisruptionBudget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a deletePolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readPolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a readPolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.readPolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a patchPolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.patchPolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPolicyV1beta1NamespacedPodDisruptionBudget(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchPolicyV1beta1NamespacedPodDisruptionBudget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replacePolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a replacePolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.replacePolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replacePolicyV1beta1NamespacedPodDisruptionBudget(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replacePolicyV1beta1NamespacedPodDisruptionBudget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readPolicyV1beta1NamespacedPodDisruptionBudgetStatus - errors', () => {
      it('should have a readPolicyV1beta1NamespacedPodDisruptionBudgetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readPolicyV1beta1NamespacedPodDisruptionBudgetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus - errors', () => {
      it('should have a patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus - errors', () => {
      it('should have a replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyV1beta1PodDisruptionBudgetForAllNamespaces - errors', () => {
      it('should have a listPolicyV1beta1PodDisruptionBudgetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicyV1beta1PodDisruptionBudgetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyV1beta1CollectionPodSecurityPolicy - errors', () => {
      it('should have a deletePolicyV1beta1CollectionPodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyV1beta1CollectionPodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a listPolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listPolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a createPolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createPolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPolicyV1beta1PodSecurityPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createPolicyV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a deletePolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readPolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a readPolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.readPolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a patchPolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchPolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPolicyV1beta1PodSecurityPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchPolicyV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replacePolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a replacePolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.replacePolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replacePolicyV1beta1PodSecurityPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replacePolicyV1beta1PodSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchPolicyV1beta1NamespacedPodDisruptionBudgetList - errors', () => {
      it('should have a watchPolicyV1beta1NamespacedPodDisruptionBudgetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchPolicyV1beta1NamespacedPodDisruptionBudgetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchPolicyV1beta1NamespacedPodDisruptionBudget - errors', () => {
      it('should have a watchPolicyV1beta1NamespacedPodDisruptionBudget function', (done) => {
        try {
          assert.equal(true, typeof a.watchPolicyV1beta1NamespacedPodDisruptionBudget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchPolicyV1beta1PodDisruptionBudgetListForAllNamespaces - errors', () => {
      it('should have a watchPolicyV1beta1PodDisruptionBudgetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchPolicyV1beta1PodDisruptionBudgetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchPolicyV1beta1PodSecurityPolicyList - errors', () => {
      it('should have a watchPolicyV1beta1PodSecurityPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.watchPolicyV1beta1PodSecurityPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchPolicyV1beta1PodSecurityPolicy - errors', () => {
      it('should have a watchPolicyV1beta1PodSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.watchPolicyV1beta1PodSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAuthorizationAPIGroup - errors', () => {
      it('should have a getRbacAuthorizationAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacAuthorizationAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAuthorizationV1APIResources - errors', () => {
      it('should have a getRbacAuthorizationV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacAuthorizationV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1CollectionClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1CollectionClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1CollectionClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1ClusterRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1CollectionClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1CollectionClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1CollectionClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a listRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a createRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a readRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a patchRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1ClusterRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1CollectionNamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1CollectionNamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1CollectionNamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1NamespacedRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1CollectionNamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1CollectionNamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1CollectionNamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a listRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a createRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a readRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a patchRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1NamespacedRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1RoleBindingForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1RoleBindingForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1RoleBindingForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1RoleForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1RoleForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1RoleForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1ClusterRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1ClusterRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1ClusterRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1ClusterRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1ClusterRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1ClusterRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1ClusterRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1ClusterRole - errors', () => {
      it('should have a watchRbacAuthorizationV1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1NamespacedRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1NamespacedRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1NamespacedRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1NamespacedRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1NamespacedRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1NamespacedRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1NamespacedRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1NamespacedRole - errors', () => {
      it('should have a watchRbacAuthorizationV1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1RoleBindingListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1RoleBindingListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1RoleBindingListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1RoleListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1RoleListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1RoleListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAuthorizationV1alpha1APIResources - errors', () => {
      it('should have a getRbacAuthorizationV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacAuthorizationV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1CollectionClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1CollectionClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1CollectionClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1alpha1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1alpha1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1alpha1ClusterRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1alpha1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1alpha1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1alpha1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1CollectionClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1CollectionClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1CollectionClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a createRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1alpha1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1alpha1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a readRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a patchRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1alpha1ClusterRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1alpha1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1alpha1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1alpha1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1CollectionNamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1CollectionNamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1CollectionNamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1alpha1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1alpha1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1alpha1NamespacedRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1alpha1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1alpha1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1alpha1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1CollectionNamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1CollectionNamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1CollectionNamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a createRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1alpha1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1alpha1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a readRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a patchRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1alpha1NamespacedRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1alpha1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1alpha1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1alpha1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1RoleBindingForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1RoleBindingForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1RoleBindingForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1alpha1RoleForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1alpha1RoleForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1alpha1RoleForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1ClusterRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1ClusterRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1ClusterRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1ClusterRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1ClusterRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1ClusterRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1ClusterRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1ClusterRole - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1NamespacedRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1NamespacedRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1NamespacedRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1NamespacedRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1NamespacedRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1NamespacedRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1NamespacedRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1NamespacedRole - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1RoleBindingListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1RoleBindingListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1RoleBindingListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1alpha1RoleListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1alpha1RoleListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1alpha1RoleListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAuthorizationV1beta1APIResources - errors', () => {
      it('should have a getRbacAuthorizationV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacAuthorizationV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1CollectionClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1CollectionClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1CollectionClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1beta1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1beta1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1beta1ClusterRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1beta1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1beta1ClusterRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1beta1ClusterRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1CollectionClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1CollectionClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1CollectionClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a listRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a createRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1beta1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1beta1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a readRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a patchRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1beta1ClusterRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1beta1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1beta1ClusterRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1beta1ClusterRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1CollectionNamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1CollectionNamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1CollectionNamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a listRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a createRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1beta1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1beta1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a readRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a patchRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1beta1NamespacedRoleBinding(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1beta1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a replaceRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1beta1NamespacedRoleBinding(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1beta1NamespacedRoleBinding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1CollectionNamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1CollectionNamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1CollectionNamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a listRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a createRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRbacAuthorizationV1beta1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createRbacAuthorizationV1beta1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a deleteRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a readRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.readRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a patchRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.patchRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchRbacAuthorizationV1beta1NamespacedRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchRbacAuthorizationV1beta1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a replaceRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceRbacAuthorizationV1beta1NamespacedRole(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceRbacAuthorizationV1beta1NamespacedRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1RoleBindingForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1beta1RoleBindingForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1RoleBindingForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRbacAuthorizationV1beta1RoleForAllNamespaces - errors', () => {
      it('should have a listRbacAuthorizationV1beta1RoleForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listRbacAuthorizationV1beta1RoleForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1ClusterRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1ClusterRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1ClusterRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1ClusterRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1ClusterRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1ClusterRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1ClusterRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1ClusterRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1ClusterRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1ClusterRole - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1ClusterRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1ClusterRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1NamespacedRoleBindingList - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1NamespacedRoleBindingList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1NamespacedRoleBindingList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1NamespacedRoleBinding - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1NamespacedRoleBinding function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1NamespacedRoleBinding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1NamespacedRoleList - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1NamespacedRoleList function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1NamespacedRoleList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1NamespacedRole - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1NamespacedRole function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1NamespacedRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1RoleBindingListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1RoleBindingListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1RoleBindingListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchRbacAuthorizationV1beta1RoleListForAllNamespaces - errors', () => {
      it('should have a watchRbacAuthorizationV1beta1RoleListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchRbacAuthorizationV1beta1RoleListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulingAPIGroup - errors', () => {
      it('should have a getSchedulingAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulingAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulingV1APIResources - errors', () => {
      it('should have a getSchedulingV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulingV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1CollectionPriorityClass - errors', () => {
      it('should have a deleteSchedulingV1CollectionPriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1CollectionPriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSchedulingV1PriorityClass - errors', () => {
      it('should have a listSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.listSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSchedulingV1PriorityClass - errors', () => {
      it('should have a createSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.createSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSchedulingV1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createSchedulingV1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1PriorityClass - errors', () => {
      it('should have a deleteSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSchedulingV1PriorityClass - errors', () => {
      it('should have a readSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.readSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSchedulingV1PriorityClass - errors', () => {
      it('should have a patchSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSchedulingV1PriorityClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchSchedulingV1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceSchedulingV1PriorityClass - errors', () => {
      it('should have a replaceSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceSchedulingV1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceSchedulingV1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1PriorityClassList - errors', () => {
      it('should have a watchSchedulingV1PriorityClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1PriorityClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1PriorityClass - errors', () => {
      it('should have a watchSchedulingV1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulingV1alpha1APIResources - errors', () => {
      it('should have a getSchedulingV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulingV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1alpha1CollectionPriorityClass - errors', () => {
      it('should have a deleteSchedulingV1alpha1CollectionPriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1alpha1CollectionPriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a listSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.listSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a createSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.createSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSchedulingV1alpha1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createSchedulingV1alpha1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a deleteSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a readSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.readSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a patchSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSchedulingV1alpha1PriorityClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchSchedulingV1alpha1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a replaceSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceSchedulingV1alpha1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceSchedulingV1alpha1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1alpha1PriorityClassList - errors', () => {
      it('should have a watchSchedulingV1alpha1PriorityClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1alpha1PriorityClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1alpha1PriorityClass - errors', () => {
      it('should have a watchSchedulingV1alpha1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1alpha1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSchedulingV1beta1APIResources - errors', () => {
      it('should have a getSchedulingV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getSchedulingV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1beta1CollectionPriorityClass - errors', () => {
      it('should have a deleteSchedulingV1beta1CollectionPriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1beta1CollectionPriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a listSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.listSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a createSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.createSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSchedulingV1beta1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createSchedulingV1beta1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a deleteSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a readSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.readSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a patchSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSchedulingV1beta1PriorityClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchSchedulingV1beta1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a replaceSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceSchedulingV1beta1PriorityClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceSchedulingV1beta1PriorityClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1beta1PriorityClassList - errors', () => {
      it('should have a watchSchedulingV1beta1PriorityClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1beta1PriorityClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSchedulingV1beta1PriorityClass - errors', () => {
      it('should have a watchSchedulingV1beta1PriorityClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchSchedulingV1beta1PriorityClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingsAPIGroup - errors', () => {
      it('should have a getSettingsAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getSettingsAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingsV1alpha1APIResources - errors', () => {
      it('should have a getSettingsV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getSettingsV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSettingsV1alpha1CollectionNamespacedPodPreset - errors', () => {
      it('should have a deleteSettingsV1alpha1CollectionNamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSettingsV1alpha1CollectionNamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a listSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.listSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a createSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.createSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSettingsV1alpha1NamespacedPodPreset(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createSettingsV1alpha1NamespacedPodPreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a deleteSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a readSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.readSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a patchSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.patchSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSettingsV1alpha1NamespacedPodPreset(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchSettingsV1alpha1NamespacedPodPreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a replaceSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.replaceSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceSettingsV1alpha1NamespacedPodPreset(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceSettingsV1alpha1NamespacedPodPreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSettingsV1alpha1PodPresetForAllNamespaces - errors', () => {
      it('should have a listSettingsV1alpha1PodPresetForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.listSettingsV1alpha1PodPresetForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSettingsV1alpha1NamespacedPodPresetList - errors', () => {
      it('should have a watchSettingsV1alpha1NamespacedPodPresetList function', (done) => {
        try {
          assert.equal(true, typeof a.watchSettingsV1alpha1NamespacedPodPresetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSettingsV1alpha1NamespacedPodPreset - errors', () => {
      it('should have a watchSettingsV1alpha1NamespacedPodPreset function', (done) => {
        try {
          assert.equal(true, typeof a.watchSettingsV1alpha1NamespacedPodPreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchSettingsV1alpha1PodPresetListForAllNamespaces - errors', () => {
      it('should have a watchSettingsV1alpha1PodPresetListForAllNamespaces function', (done) => {
        try {
          assert.equal(true, typeof a.watchSettingsV1alpha1PodPresetListForAllNamespaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStorageAPIGroup - errors', () => {
      it('should have a getStorageAPIGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getStorageAPIGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStorageV1APIResources - errors', () => {
      it('should have a getStorageV1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getStorageV1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1CollectionStorageClass - errors', () => {
      it('should have a deleteStorageV1CollectionStorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1CollectionStorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1StorageClass - errors', () => {
      it('should have a listStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1StorageClass - errors', () => {
      it('should have a createStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1StorageClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1StorageClass - errors', () => {
      it('should have a deleteStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1StorageClass - errors', () => {
      it('should have a readStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1StorageClass - errors', () => {
      it('should have a patchStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1StorageClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1StorageClass - errors', () => {
      it('should have a replaceStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1StorageClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1CollectionVolumeAttachment - errors', () => {
      it('should have a deleteStorageV1CollectionVolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1CollectionVolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1VolumeAttachment - errors', () => {
      it('should have a listStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1VolumeAttachment - errors', () => {
      it('should have a createStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1VolumeAttachment - errors', () => {
      it('should have a deleteStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1VolumeAttachment - errors', () => {
      it('should have a readStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1VolumeAttachment - errors', () => {
      it('should have a patchStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1VolumeAttachment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1VolumeAttachment - errors', () => {
      it('should have a replaceStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1VolumeAttachmentStatus - errors', () => {
      it('should have a readStorageV1VolumeAttachmentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1VolumeAttachmentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1VolumeAttachmentStatus - errors', () => {
      it('should have a patchStorageV1VolumeAttachmentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1VolumeAttachmentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1VolumeAttachmentStatus(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1VolumeAttachmentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1VolumeAttachmentStatus - errors', () => {
      it('should have a replaceStorageV1VolumeAttachmentStatus function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1VolumeAttachmentStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1VolumeAttachmentStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1VolumeAttachmentStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1StorageClassList - errors', () => {
      it('should have a watchStorageV1StorageClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1StorageClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1StorageClass - errors', () => {
      it('should have a watchStorageV1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1VolumeAttachmentList - errors', () => {
      it('should have a watchStorageV1VolumeAttachmentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1VolumeAttachmentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1VolumeAttachment - errors', () => {
      it('should have a watchStorageV1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStorageV1alpha1APIResources - errors', () => {
      it('should have a getStorageV1alpha1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getStorageV1alpha1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1alpha1CollectionVolumeAttachment - errors', () => {
      it('should have a deleteStorageV1alpha1CollectionVolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1alpha1CollectionVolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a listStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a createStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1alpha1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1alpha1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a deleteStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a readStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a patchStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1alpha1VolumeAttachment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1alpha1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a replaceStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1alpha1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1alpha1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1alpha1VolumeAttachmentList - errors', () => {
      it('should have a watchStorageV1alpha1VolumeAttachmentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1alpha1VolumeAttachmentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1alpha1VolumeAttachment - errors', () => {
      it('should have a watchStorageV1alpha1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1alpha1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStorageV1beta1APIResources - errors', () => {
      it('should have a getStorageV1beta1APIResources function', (done) => {
        try {
          assert.equal(true, typeof a.getStorageV1beta1APIResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CollectionCSIDriver - errors', () => {
      it('should have a deleteStorageV1beta1CollectionCSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CollectionCSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1beta1CSIDriver - errors', () => {
      it('should have a listStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1beta1CSIDriver - errors', () => {
      it('should have a createStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1beta1CSIDriver(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1beta1CSIDriver', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CSIDriver - errors', () => {
      it('should have a deleteStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1beta1CSIDriver - errors', () => {
      it('should have a readStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1beta1CSIDriver - errors', () => {
      it('should have a patchStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1beta1CSIDriver(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1beta1CSIDriver', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1beta1CSIDriver - errors', () => {
      it('should have a replaceStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1beta1CSIDriver(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1beta1CSIDriver', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CollectionCSINode - errors', () => {
      it('should have a deleteStorageV1beta1CollectionCSINode function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CollectionCSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1beta1CSINode - errors', () => {
      it('should have a listStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1beta1CSINode - errors', () => {
      it('should have a createStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1beta1CSINode(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1beta1CSINode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CSINode - errors', () => {
      it('should have a deleteStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1beta1CSINode - errors', () => {
      it('should have a readStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1beta1CSINode - errors', () => {
      it('should have a patchStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1beta1CSINode(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1beta1CSINode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1beta1CSINode - errors', () => {
      it('should have a replaceStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1beta1CSINode(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1beta1CSINode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CollectionStorageClass - errors', () => {
      it('should have a deleteStorageV1beta1CollectionStorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CollectionStorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1beta1StorageClass - errors', () => {
      it('should have a listStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1beta1StorageClass - errors', () => {
      it('should have a createStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1beta1StorageClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1beta1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1StorageClass - errors', () => {
      it('should have a deleteStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1beta1StorageClass - errors', () => {
      it('should have a readStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1beta1StorageClass - errors', () => {
      it('should have a patchStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1beta1StorageClass(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1beta1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1beta1StorageClass - errors', () => {
      it('should have a replaceStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1beta1StorageClass(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1beta1StorageClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1CollectionVolumeAttachment - errors', () => {
      it('should have a deleteStorageV1beta1CollectionVolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1CollectionVolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a listStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.listStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a createStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStorageV1beta1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-createStorageV1beta1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a deleteStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a readStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.readStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a patchStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.patchStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchStorageV1beta1VolumeAttachment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-patchStorageV1beta1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a replaceStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.replaceStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.replaceStorageV1beta1VolumeAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-kubernetes-adapter-replaceStorageV1beta1VolumeAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1CSIDriverList - errors', () => {
      it('should have a watchStorageV1beta1CSIDriverList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1CSIDriverList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1CSIDriver - errors', () => {
      it('should have a watchStorageV1beta1CSIDriver function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1CSIDriver === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1CSINodeList - errors', () => {
      it('should have a watchStorageV1beta1CSINodeList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1CSINodeList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1CSINode - errors', () => {
      it('should have a watchStorageV1beta1CSINode function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1CSINode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1StorageClassList - errors', () => {
      it('should have a watchStorageV1beta1StorageClassList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1StorageClassList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1StorageClass - errors', () => {
      it('should have a watchStorageV1beta1StorageClass function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1StorageClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1VolumeAttachmentList - errors', () => {
      it('should have a watchStorageV1beta1VolumeAttachmentList function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1VolumeAttachmentList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watchStorageV1beta1VolumeAttachment - errors', () => {
      it('should have a watchStorageV1beta1VolumeAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.watchStorageV1beta1VolumeAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logFileListHandler - errors', () => {
      it('should have a logFileListHandler function', (done) => {
        try {
          assert.equal(true, typeof a.logFileListHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#logFileHandler - errors', () => {
      it('should have a logFileHandler function', (done) => {
        try {
          assert.equal(true, typeof a.logFileHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCodeVersion - errors', () => {
      it('should have a getCodeVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getCodeVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
