## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Kubernetes. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Kubernetes.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Kubernetes. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getCoreAPIVersions(callback)</td>
    <td style="padding:15px">get available API versions</td>
    <td style="padding:15px">{base_path}/{version}/api/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoreV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ComponentStatus(callback)</td>
    <td style="padding:15px">list objects of kind ComponentStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/componentstatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1ComponentStatus(callback)</td>
    <td style="padding:15px">read the specified ComponentStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/componentstatuses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ConfigMapForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1EndpointsForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1EventForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1LimitRangeForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1Namespace(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1Namespace(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedBinding(body, callback)</td>
    <td style="padding:15px">create a Binding</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedConfigMap(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedConfigMap(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedConfigMap(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedConfigMap(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedConfigMap(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedConfigMap(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedConfigMap(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ConfigMap</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/configmaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedEndpoints(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedEndpoints(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedEndpoints(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedEndpoints(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedEndpoints(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedEndpoints(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedEndpoints(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Endpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/endpoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedEvent(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedEvent(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedEvent(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedEvent(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedEvent(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedEvent(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedEvent(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedLimitRange(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedLimitRange(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedLimitRange(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedLimitRange(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedLimitRange(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedLimitRange(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedLimitRange(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified LimitRange</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/limitranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedPersistentVolumeClaim(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedPersistentVolumeClaim(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedPersistentVolumeClaim(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedPersistentVolumeClaim(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPersistentVolumeClaim(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedPersistentVolumeClaim(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedPersistentVolumeClaim(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPersistentVolumeClaimStatus(callback)</td>
    <td style="padding:15px">read status of the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedPersistentVolumeClaimStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedPersistentVolumeClaimStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedPod(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedPod(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedPod(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedPod(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPod(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedPod(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedPod(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedPodAttach(callback)</td>
    <td style="padding:15px">connect GET requests to attach of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/attach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedPodAttach(callback)</td>
    <td style="padding:15px">connect POST requests to attach of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/attach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedPodBinding(body, callback)</td>
    <td style="padding:15px">create binding of a Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/binding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedPodEviction(body, callback)</td>
    <td style="padding:15px">create eviction of a Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/eviction?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedPodExec(callback)</td>
    <td style="padding:15px">connect GET requests to exec of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/exec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedPodExec(callback)</td>
    <td style="padding:15px">connect POST requests to exec of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/exec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPodLog(callback)</td>
    <td style="padding:15px">read log of the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedPodPortforward(callback)</td>
    <td style="padding:15px">connect GET requests to portforward of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/portforward?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedPodPortforward(callback)</td>
    <td style="padding:15px">connect POST requests to portforward of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/portforward?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNamespacedPodProxy(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNamespacedPodProxyWithPath(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPodStatus(callback)</td>
    <td style="padding:15px">read status of the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedPodStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedPodStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/pods/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedPodTemplate(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedPodTemplate(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedPodTemplate(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedPodTemplate(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedPodTemplate(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedPodTemplate(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedPodTemplate(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/podtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedReplicationController(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedReplicationController(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedReplicationController(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedReplicationController(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedReplicationController(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedReplicationController(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedReplicationController(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedReplicationControllerScale(callback)</td>
    <td style="padding:15px">read scale of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedReplicationControllerScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedReplicationControllerScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedReplicationControllerStatus(callback)</td>
    <td style="padding:15px">read status of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedReplicationControllerStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedReplicationControllerStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedResourceQuota(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedResourceQuota(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedResourceQuota(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedResourceQuota(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedResourceQuota(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedResourceQuota(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedResourceQuota(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedResourceQuotaStatus(callback)</td>
    <td style="padding:15px">read status of the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedResourceQuotaStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedResourceQuotaStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/resourcequotas/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedSecret(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedSecret(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedSecret(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedSecret(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedSecret(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedSecret(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedSecret(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNamespacedServiceAccount(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedServiceAccount(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedServiceAccount(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedServiceAccount(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedServiceAccount(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedServiceAccount(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedServiceAccount(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/serviceaccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1NamespacedService(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1NamespacedService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1NamespacedService(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedService(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedService(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNamespacedServiceProxy(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNamespacedServiceProxyWithPath(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/proxy/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespacedServiceStatus(callback)</td>
    <td style="padding:15px">read status of the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespacedServiceStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespacedServiceStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/services/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1Namespace(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1Namespace(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1Namespace(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1Namespace(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespaceFinalize(body, callback)</td>
    <td style="padding:15px">replace finalize of the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/finalize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NamespaceStatus(callback)</td>
    <td style="padding:15px">read status of the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NamespaceStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NamespaceStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Namespace</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/namespaces/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionNode(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1Node(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1Node(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1Node(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1Node(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1Node(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1Node(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNodeProxy(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNodeProxy(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNodeProxy(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNodeProxy(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNodeProxy(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNodeProxy(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNodeProxy(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1DeleteNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect DELETE requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1GetNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect GET requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1HeadNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect HEAD requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1OptionsNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect OPTIONS requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PatchNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect PATCH requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PostNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect POST requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectCoreV1PutNodeProxyWithPath(callback)</td>
    <td style="padding:15px">connect PUT requests to proxy of Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/proxy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1NodeStatus(callback)</td>
    <td style="padding:15px">read status of the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1NodeStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1NodeStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Node</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/nodes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1PersistentVolumeClaimForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind PersistentVolumeClaim</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1CollectionPersistentVolume(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1PersistentVolume(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoreV1PersistentVolume(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoreV1PersistentVolume(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1PersistentVolume(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1PersistentVolume(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1PersistentVolume(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoreV1PersistentVolumeStatus(callback)</td>
    <td style="padding:15px">read status of the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoreV1PersistentVolumeStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoreV1PersistentVolumeStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified PersistentVolume</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/persistentvolumes/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1PodForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Pod</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1PodTemplateForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind PodTemplate</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ReplicationControllerForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicationController</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ResourceQuotaForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ResourceQuota</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1SecretForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Secret</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ServiceAccountForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ServiceAccount</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoreV1ServiceForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Service</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1ConfigMapListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ConfigMap. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1EndpointsListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Endpoints. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1EventListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Event. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1LimitRangeListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of LimitRange. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespaceList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Namespace. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedConfigMapList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ConfigMap. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/configmaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedConfigMap(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ConfigMap. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/configmaps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedEndpointsList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Endpoints. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedEndpoints(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Endpoints. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/endpoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedEventList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Event. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedEvent(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Event. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedLimitRangeList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of LimitRange. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/limitranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedLimitRange(callback)</td>
    <td style="padding:15px">watch changes to an object of kind LimitRange. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/limitranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPersistentVolumeClaimList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PersistentVolumeClaim. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPersistentVolumeClaim(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PersistentVolumeClaim. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/persistentvolumeclaims/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPodList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Pod. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPod(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Pod. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/pods/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPodTemplateList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodTemplate. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedPodTemplate(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PodTemplate. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/podtemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedReplicationControllerList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicationController. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedReplicationController(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ReplicationController. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/replicationcontrollers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedResourceQuotaList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ResourceQuota. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedResourceQuota(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ResourceQuota. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/resourcequotas/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedSecretList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Secret. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedSecret(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Secret. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/secrets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedServiceAccountList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ServiceAccount. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedServiceAccount(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ServiceAccount. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/serviceaccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedServiceList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Service. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NamespacedService(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Service. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}/services/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1Namespace(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Namespace. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/namespaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1NodeList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Node. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1Node(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Node. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1PersistentVolumeClaimListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PersistentVolumeClaim. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/persistentvolumeclaims?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1PersistentVolumeList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PersistentVolume. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/persistentvolumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1PersistentVolume(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PersistentVolume. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/persistentvolumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1PodListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Pod. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/pods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1PodTemplateListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodTemplate. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/podtemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1ReplicationControllerListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicationController. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/replicationcontrollers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1ResourceQuotaListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ResourceQuota. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/resourcequotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1SecretListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Secret. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1ServiceAccountListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ServiceAccount. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/serviceaccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoreV1ServiceListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Service. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/api/v1/watch/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPIVersions(callback)</td>
    <td style="padding:15px">get available API versions</td>
    <td style="padding:15px">{base_path}/{version}/apis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdmissionregistrationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdmissionregistrationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdmissionregistrationV1beta1CollectionMutatingWebhookConfiguration(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdmissionregistrationV1beta1MutatingWebhookConfiguration(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdmissionregistrationV1beta1MutatingWebhookConfiguration(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdmissionregistrationV1beta1MutatingWebhookConfiguration(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAdmissionregistrationV1beta1MutatingWebhookConfiguration(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAdmissionregistrationV1beta1MutatingWebhookConfiguration(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAdmissionregistrationV1beta1MutatingWebhookConfiguration(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified MutatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/mutatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdmissionregistrationV1beta1CollectionValidatingWebhookConfiguration(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdmissionregistrationV1beta1ValidatingWebhookConfiguration(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAdmissionregistrationV1beta1ValidatingWebhookConfiguration(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdmissionregistrationV1beta1ValidatingWebhookConfiguration(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAdmissionregistrationV1beta1ValidatingWebhookConfiguration(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAdmissionregistrationV1beta1ValidatingWebhookConfiguration(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAdmissionregistrationV1beta1ValidatingWebhookConfiguration(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ValidatingWebhookConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/validatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAdmissionregistrationV1beta1MutatingWebhookConfigurationList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of MutatingWebhookConfiguration. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/watch/mutatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAdmissionregistrationV1beta1MutatingWebhookConfiguration(callback)</td>
    <td style="padding:15px">watch changes to an object of kind MutatingWebhookConfiguration. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/watch/mutatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAdmissionregistrationV1beta1ValidatingWebhookConfigurationList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ValidatingWebhookConfiguration. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/watch/validatingwebhookconfigurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAdmissionregistrationV1beta1ValidatingWebhookConfiguration(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ValidatingWebhookConfiguration. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/admissionregistration.k8s.io/v1beta1/watch/validatingwebhookconfigurations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiextensionsAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiextensionsV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiextensionsV1beta1CollectionCustomResourceDefinition(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApiextensionsV1beta1CustomResourceDefinition(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiextensionsV1beta1CustomResourceDefinition(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiextensionsV1beta1CustomResourceDefinition(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiextensionsV1beta1CustomResourceDefinition(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiextensionsV1beta1CustomResourceDefinition(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiextensionsV1beta1CustomResourceDefinition(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiextensionsV1beta1CustomResourceDefinitionStatus(callback)</td>
    <td style="padding:15px">read status of the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiextensionsV1beta1CustomResourceDefinitionStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiextensionsV1beta1CustomResourceDefinitionStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified CustomResourceDefinition</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/customresourcedefinitions/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiextensionsV1beta1CustomResourceDefinitionList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CustomResourceDefinition. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/watch/customresourcedefinitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiextensionsV1beta1CustomResourceDefinition(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CustomResourceDefinition. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiextensions.k8s.io/v1beta1/watch/customresourcedefinitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiregistrationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiregistrationV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiregistrationV1CollectionAPIService(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApiregistrationV1APIService(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiregistrationV1APIService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiregistrationV1APIService(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiregistrationV1APIService(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiregistrationV1APIService(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiregistrationV1APIService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiregistrationV1APIServiceStatus(callback)</td>
    <td style="padding:15px">read status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiregistrationV1APIServiceStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiregistrationV1APIServiceStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiregistrationV1APIServiceList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of APIService. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/watch/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiregistrationV1APIService(callback)</td>
    <td style="padding:15px">watch changes to an object of kind APIService. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1/watch/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiregistrationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiregistrationV1beta1CollectionAPIService(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApiregistrationV1beta1APIService(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApiregistrationV1beta1APIService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApiregistrationV1beta1APIService(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiregistrationV1beta1APIService(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiregistrationV1beta1APIService(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiregistrationV1beta1APIService(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readApiregistrationV1beta1APIServiceStatus(callback)</td>
    <td style="padding:15px">read status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchApiregistrationV1beta1APIServiceStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceApiregistrationV1beta1APIServiceStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified APIService</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/apiservices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiregistrationV1beta1APIServiceList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of APIService. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/watch/apiservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchApiregistrationV1beta1APIService(callback)</td>
    <td style="padding:15px">watch changes to an object of kind APIService. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apiregistration.k8s.io/v1beta1/watch/apiservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppsAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppsV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1ControllerRevisionForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1DaemonSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1DeploymentForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1CollectionNamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1NamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1NamespacedControllerRevision(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedControllerRevision(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedControllerRevision(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1CollectionNamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1NamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1NamespacedDaemonSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedDaemonSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedDaemonSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedDaemonSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedDaemonSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedDaemonSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1CollectionNamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1NamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1NamespacedDeployment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedDeployment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedDeployment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedDeploymentScale(callback)</td>
    <td style="padding:15px">read scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedDeploymentScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedDeploymentScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedDeploymentStatus(callback)</td>
    <td style="padding:15px">read status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedDeploymentStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedDeploymentStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1CollectionNamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1NamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1NamespacedReplicaSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedReplicaSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedReplicaSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedReplicaSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedReplicaSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedReplicaSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedReplicaSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedReplicaSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedReplicaSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1CollectionNamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1NamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1NamespacedStatefulSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedStatefulSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedStatefulSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedStatefulSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedStatefulSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedStatefulSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1NamespacedStatefulSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1NamespacedStatefulSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1NamespacedStatefulSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1ReplicaSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1StatefulSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1ControllerRevisionListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1DaemonSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1DeploymentListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedControllerRevisionList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedControllerRevision(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedDaemonSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedDaemonSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind DaemonSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedDeploymentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedDeployment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Deployment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedReplicaSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedReplicaSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedStatefulSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1NamespacedStatefulSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind StatefulSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1ReplicaSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1StatefulSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1/watch/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppsV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1ControllerRevisionForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1DeploymentForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1CollectionNamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1NamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta1NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1NamespacedControllerRevision(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedControllerRevision(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedControllerRevision(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1CollectionNamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1NamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1NamespacedDeployment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedDeployment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedDeployment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta1NamespacedDeploymentRollback(body, callback)</td>
    <td style="padding:15px">create rollback of a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedDeploymentScale(callback)</td>
    <td style="padding:15px">read scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedDeploymentScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedDeploymentScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedDeploymentStatus(callback)</td>
    <td style="padding:15px">read status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedDeploymentStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedDeploymentStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1CollectionNamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1NamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta1NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta1NamespacedStatefulSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedStatefulSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedStatefulSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedStatefulSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedStatefulSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedStatefulSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta1NamespacedStatefulSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta1NamespacedStatefulSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta1NamespacedStatefulSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta1StatefulSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1ControllerRevisionListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1DeploymentListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedControllerRevisionList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedControllerRevision(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedDeploymentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedDeployment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Deployment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedStatefulSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1NamespacedStatefulSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind StatefulSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta1StatefulSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta1/watch/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppsV1beta2APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2ControllerRevisionForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2DaemonSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2DeploymentForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2CollectionNamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2NamespacedControllerRevision(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta2NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2NamespacedControllerRevision(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedControllerRevision(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedControllerRevision(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedControllerRevision(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ControllerRevision</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2CollectionNamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2NamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta2NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2NamespacedDaemonSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedDaemonSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedDaemonSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedDaemonSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedDaemonSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedDaemonSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2CollectionNamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2NamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta2NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2NamespacedDeployment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedDeployment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedDeployment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedDeploymentScale(callback)</td>
    <td style="padding:15px">read scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedDeploymentScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedDeploymentScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedDeploymentStatus(callback)</td>
    <td style="padding:15px">read status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedDeploymentStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedDeploymentStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2CollectionNamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2NamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta2NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2NamespacedReplicaSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedReplicaSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedReplicaSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedReplicaSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedReplicaSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedReplicaSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedReplicaSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedReplicaSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedReplicaSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2CollectionNamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2NamespacedStatefulSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAppsV1beta2NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppsV1beta2NamespacedStatefulSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedStatefulSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedStatefulSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedStatefulSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedStatefulSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedStatefulSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedStatefulSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAppsV1beta2NamespacedStatefulSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAppsV1beta2NamespacedStatefulSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAppsV1beta2NamespacedStatefulSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/namespaces/{pathv1}/statefulsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2ReplicaSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAppsV1beta2StatefulSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind StatefulSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2ControllerRevisionListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2DaemonSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2DeploymentListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedControllerRevisionList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/controllerrevisions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedControllerRevision(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ControllerRevision. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/controllerrevisions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedDaemonSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedDaemonSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind DaemonSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedDeploymentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedDeployment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Deployment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedReplicaSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedReplicaSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedStatefulSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2NamespacedStatefulSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind StatefulSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/namespaces/{pathv1}/statefulsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2ReplicaSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAppsV1beta2StatefulSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StatefulSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/apps/v1beta2/watch/statefulsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditregistrationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditregistrationV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditregistrationV1alpha1CollectionAuditSink(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuditregistrationV1alpha1AuditSink(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuditregistrationV1alpha1AuditSink(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditregistrationV1alpha1AuditSink(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAuditregistrationV1alpha1AuditSink(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAuditregistrationV1alpha1AuditSink(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAuditregistrationV1alpha1AuditSink(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified AuditSink</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/auditsinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAuditregistrationV1alpha1AuditSinkList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of AuditSink. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/watch/auditsinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAuditregistrationV1alpha1AuditSink(callback)</td>
    <td style="padding:15px">watch changes to an object of kind AuditSink. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/auditregistration.k8s.io/v1alpha1/watch/auditsinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/authentication.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/authentication.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthenticationV1TokenReview(body, callback)</td>
    <td style="padding:15px">create a TokenReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authentication.k8s.io/v1/tokenreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/authentication.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthenticationV1beta1TokenReview(body, callback)</td>
    <td style="padding:15px">create a TokenReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authentication.k8s.io/v1beta1/tokenreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1NamespacedLocalSubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a LocalSubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1/namespaces/{pathv1}/localsubjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1SelfSubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a SelfSubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1/selfsubjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1SelfSubjectRulesReview(body, callback)</td>
    <td style="padding:15px">create a SelfSubjectRulesReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1/selfsubjectrulesreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1SubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a SubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1/subjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthorizationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1beta1NamespacedLocalSubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a LocalSubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1beta1/namespaces/{pathv1}/localsubjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1beta1SelfSubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a SelfSubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1beta1/selfsubjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1beta1SelfSubjectRulesReview(body, callback)</td>
    <td style="padding:15px">create a SelfSubjectRulesReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1beta1/selfsubjectrulesreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAuthorizationV1beta1SubjectAccessReview(body, callback)</td>
    <td style="padding:15px">create a SubjectAccessReview</td>
    <td style="padding:15px">{base_path}/{version}/apis/authorization.k8s.io/v1beta1/subjectaccessreviews?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoscalingAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoscalingV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV1HorizontalPodAutoscalerForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV1CollectionNamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV1NamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAutoscalingV1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV1NamespacedHorizontalPodAutoscaler(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV1NamespacedHorizontalPodAutoscaler(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV1NamespacedHorizontalPodAutoscalerStatus(callback)</td>
    <td style="padding:15px">read status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV1NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV1NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV1HorizontalPodAutoscalerListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/watch/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV1NamespacedHorizontalPodAutoscalerList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/watch/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV1NamespacedHorizontalPodAutoscaler(callback)</td>
    <td style="padding:15px">watch changes to an object of kind HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v1/watch/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoscalingV2beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV2beta1HorizontalPodAutoscalerForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV2beta1CollectionNamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus(callback)</td>
    <td style="padding:15px">read status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV2beta1NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta1HorizontalPodAutoscalerListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/watch/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta1NamespacedHorizontalPodAutoscalerList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/watch/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta1NamespacedHorizontalPodAutoscaler(callback)</td>
    <td style="padding:15px">watch changes to an object of kind HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta1/watch/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoscalingV2beta2APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV2beta2HorizontalPodAutoscalerForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV2beta2CollectionNamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus(callback)</td>
    <td style="padding:15px">read status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAutoscalingV2beta2NamespacedHorizontalPodAutoscalerStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified HorizontalPodAutoscaler</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta2HorizontalPodAutoscalerListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/watch/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta2NamespacedHorizontalPodAutoscalerList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/watch/namespaces/{pathv1}/horizontalpodautoscalers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchAutoscalingV2beta2NamespacedHorizontalPodAutoscaler(callback)</td>
    <td style="padding:15px">watch changes to an object of kind HorizontalPodAutoscaler. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/autoscaling/v2beta2/watch/namespaces/{pathv1}/horizontalpodautoscalers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV1JobForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV1CollectionNamespacedJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV1NamespacedJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBatchV1NamespacedJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV1NamespacedJob(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV1NamespacedJob(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV1NamespacedJob(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV1NamespacedJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV1NamespacedJobStatus(callback)</td>
    <td style="padding:15px">read status of the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV1NamespacedJobStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV1NamespacedJobStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Job</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/namespaces/{pathv1}/jobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1JobListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Job. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/watch/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1NamespacedJobList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Job. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/watch/namespaces/{pathv1}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1NamespacedJob(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Job. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1/watch/namespaces/{pathv1}/jobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV1beta1CronJobForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV1beta1CollectionNamespacedCronJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV1beta1NamespacedCronJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBatchV1beta1NamespacedCronJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV1beta1NamespacedCronJob(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV1beta1NamespacedCronJob(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV1beta1NamespacedCronJob(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV1beta1NamespacedCronJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV1beta1NamespacedCronJobStatus(callback)</td>
    <td style="padding:15px">read status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV1beta1NamespacedCronJobStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV1beta1NamespacedCronJobStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1beta1CronJobListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CronJob. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/watch/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1beta1NamespacedCronJobList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CronJob. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/watch/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV1beta1NamespacedCronJob(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CronJob. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v1beta1/watch/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBatchV2alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV2alpha1CronJobForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV2alpha1CollectionNamespacedCronJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBatchV2alpha1NamespacedCronJob(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBatchV2alpha1NamespacedCronJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBatchV2alpha1NamespacedCronJob(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV2alpha1NamespacedCronJob(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV2alpha1NamespacedCronJob(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV2alpha1NamespacedCronJob(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readBatchV2alpha1NamespacedCronJobStatus(callback)</td>
    <td style="padding:15px">read status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBatchV2alpha1NamespacedCronJobStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceBatchV2alpha1NamespacedCronJobStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified CronJob</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/namespaces/{pathv1}/cronjobs/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV2alpha1CronJobListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CronJob. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/watch/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV2alpha1NamespacedCronJobList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CronJob. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/watch/namespaces/{pathv1}/cronjobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchBatchV2alpha1NamespacedCronJob(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CronJob. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/batch/v2alpha1/watch/namespaces/{pathv1}/cronjobs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificatesAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificatesV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertificatesV1beta1CollectionCertificateSigningRequest(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCertificatesV1beta1CertificateSigningRequest(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCertificatesV1beta1CertificateSigningRequest(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCertificatesV1beta1CertificateSigningRequest(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCertificatesV1beta1CertificateSigningRequest(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCertificatesV1beta1CertificateSigningRequest(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCertificatesV1beta1CertificateSigningRequest(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCertificatesV1beta1CertificateSigningRequestApproval(body, callback)</td>
    <td style="padding:15px">replace approval of the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}/approval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCertificatesV1beta1CertificateSigningRequestStatus(callback)</td>
    <td style="padding:15px">read status of the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCertificatesV1beta1CertificateSigningRequestStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCertificatesV1beta1CertificateSigningRequestStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified CertificateSigningRequest</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/certificatesigningrequests/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCertificatesV1beta1CertificateSigningRequestList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CertificateSigningRequest. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/watch/certificatesigningrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCertificatesV1beta1CertificateSigningRequest(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CertificateSigningRequest. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/certificates.k8s.io/v1beta1/watch/certificatesigningrequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoordinationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoordinationV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoordinationV1LeaseForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoordinationV1CollectionNamespacedLease(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoordinationV1NamespacedLease(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoordinationV1NamespacedLease(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoordinationV1NamespacedLease(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoordinationV1NamespacedLease(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoordinationV1NamespacedLease(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoordinationV1NamespacedLease(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1LeaseListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Lease. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/watch/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1NamespacedLeaseList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Lease. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/watch/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1NamespacedLease(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Lease. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1/watch/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoordinationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoordinationV1beta1LeaseForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoordinationV1beta1CollectionNamespacedLease(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCoordinationV1beta1NamespacedLease(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCoordinationV1beta1NamespacedLease(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCoordinationV1beta1NamespacedLease(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCoordinationV1beta1NamespacedLease(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCoordinationV1beta1NamespacedLease(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCoordinationV1beta1NamespacedLease(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Lease</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1beta1LeaseListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Lease. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/watch/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1beta1NamespacedLeaseList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Lease. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/watch/namespaces/{pathv1}/leases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchCoordinationV1beta1NamespacedLease(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Lease. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/coordination.k8s.io/v1beta1/watch/namespaces/{pathv1}/leases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventsAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventsV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEventsV1beta1EventForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventsV1beta1CollectionNamespacedEvent(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEventsV1beta1NamespacedEvent(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEventsV1beta1NamespacedEvent(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventsV1beta1NamespacedEvent(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readEventsV1beta1NamespacedEvent(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEventsV1beta1NamespacedEvent(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceEventsV1beta1NamespacedEvent(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Event</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchEventsV1beta1EventListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Event. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/watch/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchEventsV1beta1NamespacedEventList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Event. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/watch/namespaces/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchEventsV1beta1NamespacedEvent(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Event. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/events.k8s.io/v1beta1/watch/namespaces/{pathv1}/events/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensionsAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtensionsV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1DaemonSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1DeploymentForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1IngressForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionNamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NamespacedDaemonSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1NamespacedDaemonSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedDaemonSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedDaemonSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedDaemonSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedDaemonSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedDaemonSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedDaemonSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified DaemonSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/daemonsets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionNamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NamespacedDeployment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1NamespacedDeployment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedDeployment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedDeployment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedDeployment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedDeploymentRollback(body, callback)</td>
    <td style="padding:15px">create rollback of a Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedDeploymentScale(callback)</td>
    <td style="padding:15px">read scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedDeploymentScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedDeploymentScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedDeploymentStatus(callback)</td>
    <td style="padding:15px">read status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedDeploymentStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedDeploymentStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Deployment</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/deployments/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionNamespacedIngress(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NamespacedIngress(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedIngress(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1NamespacedIngress(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedIngress(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedIngress(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedIngress(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedIngressStatus(callback)</td>
    <td style="padding:15px">read status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedIngressStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedIngressStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionNamespacedNetworkPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NamespacedNetworkPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedNetworkPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1NamespacedNetworkPolicy(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedNetworkPolicy(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedNetworkPolicy(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedNetworkPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionNamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NamespacedReplicaSet(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1NamespacedReplicaSet(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedReplicaSet(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedReplicaSet(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedReplicaSet(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedReplicaSetScale(callback)</td>
    <td style="padding:15px">read scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedReplicaSetScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedReplicaSetScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedReplicaSetStatus(callback)</td>
    <td style="padding:15px">read status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedReplicaSetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedReplicaSetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicasets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1NamespacedReplicationControllerDummyScale(callback)</td>
    <td style="padding:15px">read scale of the specified ReplicationControllerDummy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1NamespacedReplicationControllerDummyScale(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update scale of the specified ReplicationControllerDummy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1NamespacedReplicationControllerDummyScale(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace scale of the specified ReplicationControllerDummy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/namespaces/{pathv1}/replicationcontrollers/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1NetworkPolicyForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1CollectionPodSecurityPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1PodSecurityPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtensionsV1beta1PodSecurityPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtensionsV1beta1PodSecurityPolicy(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readExtensionsV1beta1PodSecurityPolicy(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtensionsV1beta1PodSecurityPolicy(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceExtensionsV1beta1PodSecurityPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensionsV1beta1ReplicaSetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind ReplicaSet</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1DaemonSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1DeploymentListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1IngressListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Ingress. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedDaemonSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of DaemonSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/daemonsets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedDaemonSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind DaemonSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/daemonsets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedDeploymentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Deployment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedDeployment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Deployment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/deployments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedIngressList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Ingress. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedIngress(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Ingress. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedNetworkPolicyList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedNetworkPolicy(callback)</td>
    <td style="padding:15px">watch changes to an object of kind NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedReplicaSetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NamespacedReplicaSet(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/namespaces/{pathv1}/replicasets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1NetworkPolicyListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1PodSecurityPolicyList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodSecurityPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1PodSecurityPolicy(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PodSecurityPolicy. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchExtensionsV1beta1ReplicaSetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ReplicaSet. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/extensions/v1beta1/watch/replicasets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkingAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkingV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkingV1CollectionNamespacedNetworkPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkingV1NamespacedNetworkPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkingV1NamespacedNetworkPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkingV1NamespacedNetworkPolicy(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readNetworkingV1NamespacedNetworkPolicy(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworkingV1NamespacedNetworkPolicy(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNetworkingV1NamespacedNetworkPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkingV1NetworkPolicyForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind NetworkPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1NamespacedNetworkPolicyList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/watch/namespaces/{pathv1}/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1NamespacedNetworkPolicy(callback)</td>
    <td style="padding:15px">watch changes to an object of kind NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/watch/namespaces/{pathv1}/networkpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1NetworkPolicyListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of NetworkPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1/watch/networkpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkingV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkingV1beta1IngressForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkingV1beta1CollectionNamespacedIngress(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkingV1beta1NamespacedIngress(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkingV1beta1NamespacedIngress(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create an Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkingV1beta1NamespacedIngress(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete an Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readNetworkingV1beta1NamespacedIngress(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworkingV1beta1NamespacedIngress(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNetworkingV1beta1NamespacedIngress(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readNetworkingV1beta1NamespacedIngressStatus(callback)</td>
    <td style="padding:15px">read status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetworkingV1beta1NamespacedIngressStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNetworkingV1beta1NamespacedIngressStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified Ingress</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/namespaces/{pathv1}/ingresses/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1beta1IngressListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Ingress. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/watch/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1beta1NamespacedIngressList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Ingress. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/watch/namespaces/{pathv1}/ingresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNetworkingV1beta1NamespacedIngress(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Ingress. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/networking.k8s.io/v1beta1/watch/namespaces/{pathv1}/ingresses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeV1alpha1CollectionRuntimeClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNodeV1alpha1RuntimeClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNodeV1alpha1RuntimeClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeV1alpha1RuntimeClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readNodeV1alpha1RuntimeClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNodeV1alpha1RuntimeClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNodeV1alpha1RuntimeClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNodeV1alpha1RuntimeClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RuntimeClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/watch/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNodeV1alpha1RuntimeClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind RuntimeClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1alpha1/watch/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeV1beta1CollectionRuntimeClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNodeV1beta1RuntimeClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNodeV1beta1RuntimeClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeV1beta1RuntimeClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readNodeV1beta1RuntimeClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNodeV1beta1RuntimeClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNodeV1beta1RuntimeClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified RuntimeClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNodeV1beta1RuntimeClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RuntimeClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/watch/runtimeclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchNodeV1beta1RuntimeClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind RuntimeClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/node.k8s.io/v1beta1/watch/runtimeclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyV1beta1CollectionNamespacedPodDisruptionBudget(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyV1beta1NamespacedPodDisruptionBudget(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyV1beta1NamespacedPodDisruptionBudget(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyV1beta1NamespacedPodDisruptionBudget(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readPolicyV1beta1NamespacedPodDisruptionBudget(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPolicyV1beta1NamespacedPodDisruptionBudget(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replacePolicyV1beta1NamespacedPodDisruptionBudget(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readPolicyV1beta1NamespacedPodDisruptionBudgetStatus(callback)</td>
    <td style="padding:15px">read status of the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPolicyV1beta1NamespacedPodDisruptionBudgetStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replacePolicyV1beta1NamespacedPodDisruptionBudgetStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyV1beta1PodDisruptionBudgetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind PodDisruptionBudget</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyV1beta1CollectionPodSecurityPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPolicyV1beta1PodSecurityPolicy(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyV1beta1PodSecurityPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyV1beta1PodSecurityPolicy(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readPolicyV1beta1PodSecurityPolicy(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPolicyV1beta1PodSecurityPolicy(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replacePolicyV1beta1PodSecurityPolicy(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PodSecurityPolicy</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchPolicyV1beta1NamespacedPodDisruptionBudgetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodDisruptionBudget. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/watch/namespaces/{pathv1}/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchPolicyV1beta1NamespacedPodDisruptionBudget(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PodDisruptionBudget. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/watch/namespaces/{pathv1}/poddisruptionbudgets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchPolicyV1beta1PodDisruptionBudgetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodDisruptionBudget. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/watch/poddisruptionbudgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchPolicyV1beta1PodSecurityPolicyList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodSecurityPolicy. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/watch/podsecuritypolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchPolicyV1beta1PodSecurityPolicy(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PodSecurityPolicy. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/policy/v1beta1/watch/podsecuritypolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacAuthorizationAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacAuthorizationV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1CollectionClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1ClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1ClusterRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">read the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1ClusterRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1CollectionClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1ClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1ClusterRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1ClusterRole(callback)</td>
    <td style="padding:15px">read the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1ClusterRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1CollectionNamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1NamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1NamespacedRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">read the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1NamespacedRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1CollectionNamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1NamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1NamespacedRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1NamespacedRole(callback)</td>
    <td style="padding:15px">read the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1NamespacedRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1RoleBindingForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1RoleForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1ClusterRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1ClusterRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRole. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1ClusterRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRole. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1NamespacedRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind RoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1NamespacedRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1NamespacedRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Role. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1RoleBindingListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1RoleListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1/watch/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacAuthorizationV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1CollectionClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1ClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1alpha1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1ClusterRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1alpha1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">read the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1alpha1ClusterRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1alpha1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1CollectionClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1ClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1alpha1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1ClusterRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1alpha1ClusterRole(callback)</td>
    <td style="padding:15px">read the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1alpha1ClusterRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1alpha1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1CollectionNamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1NamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1alpha1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1NamespacedRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1alpha1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">read the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1alpha1NamespacedRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1alpha1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1CollectionNamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1NamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1alpha1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1alpha1NamespacedRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1alpha1NamespacedRole(callback)</td>
    <td style="padding:15px">read the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1alpha1NamespacedRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1alpha1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1RoleBindingForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1alpha1RoleForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1ClusterRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1ClusterRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRole. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1ClusterRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRole. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1NamespacedRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind RoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1NamespacedRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1NamespacedRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Role. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1RoleBindingListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1alpha1RoleListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1alpha1/watch/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacAuthorizationV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1CollectionClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1ClusterRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1beta1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1ClusterRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1beta1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">read the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1beta1ClusterRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1beta1ClusterRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1CollectionClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1ClusterRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1beta1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1ClusterRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1beta1ClusterRole(callback)</td>
    <td style="padding:15px">read the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1beta1ClusterRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1beta1ClusterRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified ClusterRole</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1CollectionNamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1NamespacedRoleBinding(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1beta1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1NamespacedRoleBinding(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1beta1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">read the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1beta1NamespacedRoleBinding(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1beta1NamespacedRoleBinding(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1CollectionNamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1NamespacedRole(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacAuthorizationV1beta1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAuthorizationV1beta1NamespacedRole(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readRbacAuthorizationV1beta1NamespacedRole(callback)</td>
    <td style="padding:15px">read the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchRbacAuthorizationV1beta1NamespacedRole(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRbacAuthorizationV1beta1NamespacedRole(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1RoleBindingForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind RoleBinding</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRbacAuthorizationV1beta1RoleForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind Role</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1ClusterRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/clusterrolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1ClusterRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/clusterrolebindings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1ClusterRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of ClusterRole. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/clusterroles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1ClusterRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind ClusterRole. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/clusterroles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1NamespacedRoleBindingList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/namespaces/{pathv1}/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1NamespacedRoleBinding(callback)</td>
    <td style="padding:15px">watch changes to an object of kind RoleBinding. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/namespaces/{pathv1}/rolebindings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1NamespacedRoleList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/namespaces/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1NamespacedRole(callback)</td>
    <td style="padding:15px">watch changes to an object of kind Role. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/namespaces/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1RoleBindingListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of RoleBinding. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/rolebindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchRbacAuthorizationV1beta1RoleListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of Role. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/rbac.authorization.k8s.io/v1beta1/watch/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulingAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulingV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1CollectionPriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSchedulingV1PriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSchedulingV1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1PriorityClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSchedulingV1PriorityClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSchedulingV1PriorityClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSchedulingV1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1PriorityClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PriorityClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/watch/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1PriorityClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PriorityClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1/watch/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulingV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1alpha1CollectionPriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSchedulingV1alpha1PriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSchedulingV1alpha1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1alpha1PriorityClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSchedulingV1alpha1PriorityClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSchedulingV1alpha1PriorityClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSchedulingV1alpha1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1alpha1PriorityClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PriorityClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/watch/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1alpha1PriorityClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PriorityClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1alpha1/watch/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulingV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1beta1CollectionPriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSchedulingV1beta1PriorityClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSchedulingV1beta1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulingV1beta1PriorityClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSchedulingV1beta1PriorityClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSchedulingV1beta1PriorityClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSchedulingV1beta1PriorityClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PriorityClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1beta1PriorityClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PriorityClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/watch/priorityclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSchedulingV1beta1PriorityClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PriorityClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/scheduling.k8s.io/v1beta1/watch/priorityclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingsAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingsV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSettingsV1alpha1CollectionNamespacedPodPreset(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSettingsV1alpha1NamespacedPodPreset(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSettingsV1alpha1NamespacedPodPreset(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSettingsV1alpha1NamespacedPodPreset(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSettingsV1alpha1NamespacedPodPreset(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSettingsV1alpha1NamespacedPodPreset(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceSettingsV1alpha1NamespacedPodPreset(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/namespaces/{pathv1}/podpresets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSettingsV1alpha1PodPresetForAllNamespaces(callback)</td>
    <td style="padding:15px">list or watch objects of kind PodPreset</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSettingsV1alpha1NamespacedPodPresetList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodPreset. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/watch/namespaces/{pathv1}/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSettingsV1alpha1NamespacedPodPreset(callback)</td>
    <td style="padding:15px">watch changes to an object of kind PodPreset. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/watch/namespaces/{pathv1}/podpresets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchSettingsV1alpha1PodPresetListForAllNamespaces(callback)</td>
    <td style="padding:15px">watch individual changes to a list of PodPreset. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/settings.k8s.io/v1alpha1/watch/podpresets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageAPIGroup(callback)</td>
    <td style="padding:15px">get information of a group</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageV1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1CollectionStorageClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1StorageClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1StorageClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1StorageClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1StorageClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1StorageClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1StorageClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1CollectionVolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1VolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1VolumeAttachment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1VolumeAttachment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1VolumeAttachment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1VolumeAttachmentStatus(callback)</td>
    <td style="padding:15px">read status of the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1VolumeAttachmentStatus(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update status of the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1VolumeAttachmentStatus(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace status of the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/volumeattachments/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1StorageClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StorageClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/watch/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1StorageClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind StorageClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/watch/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1VolumeAttachmentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/watch/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1VolumeAttachment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1/watch/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageV1alpha1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1alpha1CollectionVolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1alpha1VolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1alpha1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1alpha1VolumeAttachment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1alpha1VolumeAttachment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1alpha1VolumeAttachment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1alpha1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1alpha1VolumeAttachmentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/watch/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1alpha1VolumeAttachment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1alpha1/watch/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageV1beta1APIResources(callback)</td>
    <td style="padding:15px">get available resources</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CollectionCSIDriver(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1beta1CSIDriver(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1beta1CSIDriver(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CSIDriver(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1beta1CSIDriver(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1beta1CSIDriver(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1beta1CSIDriver(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CSIDriver</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csidrivers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CollectionCSINode(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1beta1CSINode(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1beta1CSINode(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CSINode(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1beta1CSINode(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1beta1CSINode(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1beta1CSINode(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified CSINode</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/csinodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CollectionStorageClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1beta1StorageClass(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1beta1StorageClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1StorageClass(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1beta1StorageClass(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1beta1StorageClass(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1beta1StorageClass(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified StorageClass</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1CollectionVolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">delete collection of VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listStorageV1beta1VolumeAttachment(allowWatchBookmarks, continueParam, fieldSelector, labelSelector, limit, resourceVersion, timeoutSeconds, watch, callback)</td>
    <td style="padding:15px">list or watch objects of kind VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStorageV1beta1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">create a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStorageV1beta1VolumeAttachment(body, dryRun, gracePeriodSeconds, orphanDependents, propagationPolicy, callback)</td>
    <td style="padding:15px">delete a VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readStorageV1beta1VolumeAttachment(exact, exportParam, callback)</td>
    <td style="padding:15px">read the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchStorageV1beta1VolumeAttachment(body, dryRun, fieldManager, force, callback)</td>
    <td style="padding:15px">partially update the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceStorageV1beta1VolumeAttachment(body, dryRun, fieldManager, callback)</td>
    <td style="padding:15px">replace the specified VolumeAttachment</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1CSIDriverList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CSIDriver. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/csidrivers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1CSIDriver(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CSIDriver. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/csidrivers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1CSINodeList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of CSINode. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/csinodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1CSINode(callback)</td>
    <td style="padding:15px">watch changes to an object of kind CSINode. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/csinodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1StorageClassList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of StorageClass. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/storageclasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1StorageClass(callback)</td>
    <td style="padding:15px">watch changes to an object of kind StorageClass. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/storageclasses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1VolumeAttachmentList(callback)</td>
    <td style="padding:15px">watch individual changes to a list of VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/volumeattachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watchStorageV1beta1VolumeAttachment(callback)</td>
    <td style="padding:15px">watch changes to an object of kind VolumeAttachment. deprecated: use the 'watch' parameter with a list operation instead, filtered to a single item with the 'fieldSelector' parameter.</td>
    <td style="padding:15px">{base_path}/{version}/apis/storage.k8s.io/v1beta1/watch/volumeattachments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logFileListHandler(callback)</td>
    <td style="padding:15px">logFileListHandler</td>
    <td style="padding:15px">{base_path}/{version}/logs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logFileHandler(callback)</td>
    <td style="padding:15px">logFileHandler</td>
    <td style="padding:15px">{base_path}/{version}/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCodeVersion(callback)</td>
    <td style="padding:15px">get the code version</td>
    <td style="padding:15px">{base_path}/{version}/version/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
