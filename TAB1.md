# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Kubernetes System. The API that was used to build the adapter for Kubernetes is usually available in the report directory of this adapter. The adapter utilizes the Kubernetes API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Kubernetes adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Kubernetes. With this adapter you have the ability to perform operations such as:

- Service Discovery and Load Balancing: Kubernetes can expose a container using the DNS name or using its own IP address. If traffic to a container is high, automatically distribute the network traffic so that the deployment is stable.

- Storage Orchestration: Automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.

- Automated Rollouts and Rollbacks: You can describe the desired state for your deployed containers, and it can change the actual state to the desired state at a controlled rate.

- Automatic Bin Packing: Automatically run containerized tasks with descriptions of how much CPU and memory (RAM) each container needs. Kubernetes can fit containers onto your nodes to make the best use of your resources.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
