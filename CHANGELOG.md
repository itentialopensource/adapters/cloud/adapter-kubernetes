
## 0.9.4 [10-15-2024]

* Changes made at 2024.10.14_21:34PM

See merge request itentialopensource/adapters/adapter-kubernetes!20

---

## 0.9.3 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-kubernetes!18

---

## 0.9.2 [08-15-2024]

* Changes made at 2024.08.14_19:54PM

See merge request itentialopensource/adapters/adapter-kubernetes!17

---

## 0.9.1 [08-07-2024]

* Changes made at 2024.08.06_22:02PM

See merge request itentialopensource/adapters/adapter-kubernetes!16

---

## 0.9.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!15

---

## 0.8.5 [03-28-2024]

* Changes made at 2024.03.28_13:28PM

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!14

---

## 0.8.4 [03-11-2024]

* Changes made at 2024.03.11_15:39PM

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!13

---

## 0.8.3 [02-28-2024]

* Changes made at 2024.02.28_11:55AM

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!12

---

## 0.8.2 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!11

---

## 0.8.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!10

---

## 0.8.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!9

---

## 0.7.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!8

---

## 0.6.3 [03-09-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!7

---

## 0.6.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!6

---

## 0.6.1 [01-13-2020]

- Bring the adapter up to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!5

---

## 0.6.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!4

---

## 0.5.0 [09-17-2019] & 0.4.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!3

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorization and prepare for app artifact.

See merge request itentialopensource/adapters/cloud/adapter-kubernetes!2

---

## 0.1.2 [06-04-2019]

- Ran the migrator - some permission changes but otherwise only re formatting of propertiesSchema.json

See merge request itentialopensource/adapters/adapter-kubernetes!1

---

## 0.1.1 [06-03-2019]

- Initial Commit

See commit 8cc599c

---
