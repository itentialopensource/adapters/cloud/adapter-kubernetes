# Kubernetes

Vendor: Kubernetes
Homepage: https://kubernetes.io/

Product: Kubernetes
Product Page: https://kubernetes.io/

## Introduction
We classify Kubernetes into the Cloud domain as Kubernetes provides the capability to deploy, manage and orchestrate containerized applications in cloud environments. We also classify it into the Data Center domain because it provides the container orchestration capabilities within data center environments.

## Why Integrate
The Kubernetes adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Kubernetes. With this adapter you have the ability to perform operations such as:

- Service Discovery and Load Balancing: Kubernetes can expose a container using the DNS name or using its own IP address. If traffic to a container is high, automatically distribute the network traffic so that the deployment is stable.

- Storage Orchestration: Automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.

- Automated Rollouts and Rollbacks: You can describe the desired state for your deployed containers, and it can change the actual state to the desired state at a controlled rate.

- Automatic Bin Packing: Automatically run containerized tasks with descriptions of how much CPU and memory (RAM) each container needs. Kubernetes can fit containers onto your nodes to make the best use of your resources.

## Additional Product Documentation
The [API documents for Kubernetes](https://kubernetes.io/docs/reference/)

